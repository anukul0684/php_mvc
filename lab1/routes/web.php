<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

//Calling the Home page of the static website
Route::get('/','PageController@home');

//Calling services page of the static website
Route::get('/services','PageController@services');

//Calling the about us page of the static website
Route::get('/aboutus','PageController@aboutus');

//Calling the contact us page of the static website
Route::get('/contactus','PageController@contactus');

//calling the courses page of the static website
Route::get('/courses','PageController@courses');

//calling the services at home page of the static website
Route::get('/servicesathome','PageController@servicesathome');

//calling the seminars page of the static website
Route::get('/seminars','PageController@seminars');

//calling the products page of the static website
Route::get('/products','PageController@products');

//calling the specials page of the static website
Route::get('/specials','PageController@specials');
