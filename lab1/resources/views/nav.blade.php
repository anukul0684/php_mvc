<nav id="desktopnav">      
      <ul id="main_navigation">      
          <li class="main_nav"><a title="Home Page" href="/">HOME</a></li>
          <li class="main_nav"><a title="Angel Services" href="/services">SERVICES</a></li>
          <li class="sub_menu main_nav"><a title="Angel Specials" href="/specials">SPECIALS</a>
            <ul id="sub_list">
              <li><a title="Angel Courses" href="/courses">COURSES</a></li>
              <li><a title="Angel Seminars" href="/seminars">SEMINARS</a></li>
              <li><a title="Angel Services at Home" href="/servicesathome">SERVICES @ HOME</a></li>
              <li><a title="Angel Products" href="/products">PRODUCTS</a></li>
            </ul>
          </li>
          <li class="current_page main_nav"><a title="About Us" href="/aboutus">ABOUT US</a></li>
          <li class="main_nav"><a title="Contact Us" href="/contactus">CONTACT</a></li>                   
      </ul>      
    </nav>
    
    <!-- Semnatic Element Nav for navigation menu in mobile view -->
    <nav id="mobilenav">
      <a title="Click for Menu" href="#" id="mobile_menu">
          <span id="topbar"></span>
          <span id="middlebar"></span>
          <span id="bottombar"></span>
      </a>
      <ul id="mobile_navigation">      
          <li><a title="Home Page" href="/index">HOME</a></li>
          <li><a title="Angel Services" href="/services">SERVICES</a></li>
          <li><a title="Angel Specials" href="/specials">SPECIALS</a>
            <ul id="mobilesub_list">
              <li><a title="Angel Courses" href="/courses">COURSES</a></li>
              <li><a title="Angel Seminars" href="/seminars">SEMINARS</a></li>
              <li><a title="Angel Services at Home" href="/servicesathome">SERVICES @ HOME</a></li>
              <li><a title="Angel Products" href="/products">PRODUCTS</a></li>
            </ul>
          </li>
          <li class="current_page"><a title="About Us" href="/aboutus">ABOUT US</a></li>
          <li><a title="Contact Us" href="/contactus">CONTACT</a></li>                   
      </ul>
    </nav>