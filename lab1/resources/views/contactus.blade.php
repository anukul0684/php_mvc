<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <!--

    ///////////////////////////////////////////////////////////////////////
    //                                                                   //
    //                  Program Name: Web Development                    //
    //                   Course Name: PHP MVC Assignment 1               //
    //                         Batch: WDD7 - Jan 2020                    //
    //                    Instructor: Steve George                       //
    //   HTML5/CSS3 Capstone Project: contactus.html                     // 
    //                                                                   //
    //                  Submitted by: Anu Kulshrestha                    //
    //                                                                   //
    ///////////////////////////////////////////////////////////////////////

  -->
  
  <!-- title of page that will be shown in tab of the browser-->
  <title>Angel ContactUs</title>
  
  <!-- code to make page responsive-->
  <meta name="viewport" content="width=device-width, initial-scale=1"  />
  
  <!-- link to javascript file for IE8 -->
  <script src="JS_files/old_ie.js"></script>
  
  <!-- fav icon -->
  
  <link rel="icon" href="images/favicon.png" type="image/x-icon"/>
  <!--    icons for touch devices apple, android etc.-->
  <link rel="apple-touch-icon" sizes="128x128" href="images/favicon-128.png"/>
  <link rel="apple-touch-icon" sizes="152x152" href="images/favicon-152.png"/>
  <link rel="apple-touch-icon" sizes="167x167" href="images/favicon-167.png"/>
  <link rel="apple-touch-icon" sizes="180x180" href="images/favicon-180.png"/>
  <link rel="apple-touch-icon" sizes="196x196" href="images/favicon-196.png"/>
  <link rel="shortcut icon" sizes="196x196" href="images/favicon-196.png" />
  <link rel="icon" type="image/x-icon" href="images/favicon.ico" />
  <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon"/>
  
  <!-- link to css files for desktop, mobile and print view -->
  <link rel="stylesheet" type="text/css" media="screen and (min-width:768px)" href="styles/as_desktop_styles.css"/>
  <link rel="stylesheet" type="text/css" media="screen and (max-width:767px)" href="styles/as_mobile_styles.css"/>
  <link rel="stylesheet" type="text/css" media="print" href="styles/as_print_styles.css"/>
  
  <!-- link to google fonts for the website content-->
  <link href="https://fonts.googleapis.com/css2?family=Charmonman:wght@700&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,500&display=swap" rel="stylesheet">
  
  <!-- Conditional Comments for version of Internet Explorer 8 and lower
         to understand that following HTML5 elements should be block. -->         
    
  <!--[if LTE IE 9]>
    <style type="text/css">
      #page_content{
        font-family: 'Times New Roman', serif;
        font-size: 16px;
        font-weight: 400;
        max-width: 960px;
        margin: 0 auto;
        position: relative;
        box-sizing: border-box;   
        background: linear-gradient(#D1D06B, #eee, #C1C05A, #D1D06B);
      }
      
      header{
        height: 140px;
        padding-top: 2px;        
      }
      
      header{
        background-color: #D1D06E; 
      }    
      
      header div{
        float: right;
        margin-top: 10px;
      }
         
      header a.social_mlinks{
        text-decoration: none;
      }

      header a.social_mlinks img{           
        padding-right: 5px;  
        border: 2px solid #D1D06E;
      }
          
      header a img#logo{
        float: left; 
        width: 130px;
        height: 130px;
        border: 2px solid #D1D06E;        
      }
      
      header div#head_msg{
        visibility: visible;    
        font-size: 0.9em;
        float: right;
        margin-right: 5px;
        margin-top: -15px;
      }
      
      h1{
        float: left;
        text-align: right;        
        font-size: 3em;    
        margin-bottom: 0;
        font-weight:500;
      }
           
      h1 a{
        text-decoration:none;
      }
            
      header, nav, section, footer{
        display:block;
        box-sizing: border-box;
      }
      
      nav{
        height: 37px;
        padding-top: 1px;
      }
      
      nav ul#main_navigation{
        float: left;
        margin-left: 200px;
      }
      
      nav ul#main_navigation li.main_nav a{
        text-decoration:none;
        padding: 0 10px 0 10px;        
      }
        
      nav, footer{
        background-color: #ddd;
      }
            
      nav#mobilenav{
        display: none;
      }
      
      nav#desktopnav ul#sub_list{
        display: none;
      }
      
      nav#desktopnav li{
        display: inline;
      } 
      
      section{
        background-color: #eec;
        min-height: 650px;
        padding-bottom: 1px; 
        padding-top: 1px;             
      }
      
      a.breadcrumb{
        float: left;        
      }
      
      h2{
        text-align: center;
      }
       
      section #contactus_container{
        min-height: 650px;
        padding-top: 1px;     
        text-align:center;     
      }
        
      section #formobile{
        float: left;
        margin-top: -55px;
        margin-left: 40px;        
        text-decoration: none;
      }
      
      footer{
        min-height: 160px;
        padding-top: 1px;
      }
      
      footer ul{ 
        float: left;
        margin-left: 70px;
        margin-top: 10px;        
      }
      
      footer ul li{
        display: inline-block;        
      }
      
      footer ul li a{
        text-decoration:none;
        color: #333;
        padding: 0 10px 0 10px;
      }
      
      footer div#site_details{
        display: inline-block;
        float: left;
        margin-top: 10px;
        margin-left: 300px;
        text-align: center;
      }    
    </style>
  <![endif]-->  
    
</head>
<body>
 
  <!-- A Div to contain all elements and position them in width of 960px with a middle view to viewport -->
  <div id="page_content">
   
    <!-- Semnatic Element Header for logo, social links and company name -->
    <header>      
      <a title="logo image" href="index.html">     
        <picture>        
          <source media="(max-width:767px)" title="mobile logo" srcset="images/whiteflower_fs.png" />
          <source media="(min-width:768px)" title="desktop logo" srcset="images/whiteflower_f1.png" />
          <img id="logo" src="images/whiteflower_f1.png" title="logo image" alt="fall_back" />
        </picture>
      </a>
      <h1><a title="Company Name" href="index.html">Angel Salon </a></h1>
      <div id="slink_image">               
        <a title="fb" class="social_mlinks" href="#" ><img src="images/fb.png" title="fb page" alt="facebook" /> </a>
        <a title="twitter" class="social_mlinks" href="#" ><img src="images/twitter.png" title="twitter feeds" alt="twitter feeds" /> </a>
        <a title="Instagram" class="social_mlinks" href="#" ><img src="images/insta.png" title="insta posts" alt="instagram" /> </a>
      </div>
      <div id="head_msg">
        <div id="displaymsg1">
            Call at (204)-599-8236 for booking an appointment.
        </div>
        <div id="displaymsg2">
            Salon Timings: 10 a.m. till 5 p.m. Mondays closed.
        </div> 
      </div>
    </header>
    
    <!-- Semnatic Element Nav for navigation menu in desktop view -->
    <nav id="desktopnav">      
      <ul id="main_navigation">      
          <li class="main_nav"><a title="Home Page" href="/">HOME</a></li>
          <li class="main_nav"><a title="Angel Services" href="/services">SERVICES</a></li>
          <li class="sub_menu main_nav"><a title="Angel Specials" href="/specials">SPECIALS</a>
            <ul id="sub_list">
              <li><a title="Angel Courses" href="/courses">COURSES</a></li>
              <li><a title="Angel Seminars" href="/seminars">SEMINARS</a></li>
              <li><a title="Angel Services at Home" href="/servicesathome">SERVICES @ HOME</a></li>
              <li><a title="Angel Products" href="/products">PRODUCTS</a></li>
            </ul>
          </li>
          <li class="main_nav"><a title="About Us" href="/aboutus">ABOUT US</a></li>
          <li class="current_page main_nav"><a title="Contact Us" href="/contactus">CONTACT</a></li>                   
      </ul>      
    </nav>
    
    <!-- Semnatic Element Nav for navigation menu in mobile view -->
    <nav id="mobilenav">
      <a title="Click for Menu" href="#" id="mobile_menu">
          <span id="topbar"></span>
          <span id="middlebar"></span>
          <span id="bottombar"></span>
      </a>
      <ul id="mobile_navigation">      
          <li><a title="Home Page" href="/">HOME</a></li>
          <li><a title="Angel Services" href="/services">SERVICES</a></li>
          <li><a title="Angel Specials" href="/specials">SPECIALS</a>
            <ul id="mobilesub_list">
              <li><a title="Angel Courses" href="/courses">COURSES</a></li>
              <li><a title="Angel Seminars" href="/seminars">SEMINARS</a></li>
              <li><a title="Angel Services at Home" href="/servicesathome">SERVICES @ HOME</a></li>
              <li><a title="Angel Products" href="/products">PRODUCTS</a></li>
            </ul>
          </li>
          <li><a title="About Us" href="/aboutus">ABOUT US</a></li>
          <li class="current_page"><a title="Contact Us" href="/contactus">CONTACT</a></li>                   
      </ul>
    </nav>
    
    <!-- Semnatic Element Section for main contents serving the purpose of the page -->
    <section>
       
      <!-- Link for mobile view to call for booking-->     
      <p>
        <a title="Book Appointment" id="formobile" href="tel:1-204-599-8236">Book an Appointment <br/> @ 204-599-8236</a>
      </p>

      <!-- Bread crumbs to reach back from where one has come -->
      <div id="webpage_path">
        <a title="Home Page" class="breadcrumb" style="margin-left: 10px; color: #660505;" href="/">Home</a> > Contact Us 
      </div>
      <div id="contact_main">
        <div id="salonaddress">           
          <address>
            Our Address:
            1234, Sing Along Avenue,
            Winnipeg,
            R9C 8Z1
          </address>
        </div>
        <!-- Div container for the content of Contact form -->
        <div id="contactus_container">
          <h2>Contact us with below details:</h2>
          <form id="contact_form"
            method="post"
            action="http://www.scott-media.com/test/form_display.php" 
            autocomplete="on">          

            <!-- contact details -->
            <fieldset id="personalinfo">
              <legend>Contact Details</legend>

              <!-- Person's Name. for referring back -->
              <p>
                <label class="info" for="Name">Name :</label> 
                <input type="text" 
                       name="Name"
                       id="Name" 
                       required="required"
                       maxlength="40" 
                       size="20" 
                       placeholder="please enter your name"/> 
              </p>                

              <!-- Person's Mobile number for calling for appointment-->
              <p>
                <label class="info" for="mob_number">Contact Number (M) :</label>
                <input type="tel"
                       name="mob_number"
                       id="mob_number" 
                       required="required"
                       placeholder="please enter your mobile no." 
                       autocomplete="off"/>
              </p>           

              <!-- Person's email id. for sending info -->
              <p>
                <label class="info" for="email_address">Registered Email Id :</label>
                <input type="email" 
                       name="email_address"
                       id="email_address"
                       required="required"
                       placeholder="please enter your email-id"/> 
              </p>

              <!-- date preferred for booking -->
              <p>
                <label class="info" for="booking_date">Booking Date You Prefer :</label>
                <input type="date"
                       name="booking_date"
                       id="booking_date" /> 
              </p> 

              <!-- time preferred for contacting back -->
              <p> 
                <label class="info" for="contact_time">Preferred Time for Contact :</label>
                <select id="contact_time" name="contact_time">
                  <option value="no preference">Any time</option>            
                  <option value="morning">Before 10 a.m.</option>
                  <option value="daytime">Between 10 a.m. and 5 p.m.</option>
                  <option value="evening">after 5 p.m.</option>
                  <option value="weekend">on weekend</option>                        
                </select>
              </p>   

              <!-- Queries for Salon -->
              <p> 
                <label class="info" for="query">Queries :</label> 
                <textarea name="query" 
                          id="query" 
                          cols="34"
                          rows="3" 
                          placeholder="please enter your queries here."></textarea>          
              </p>   
            </fieldset>
            <!-- Action buttons -->
            <p id="sent">
              <input type="submit" 
                     id="subreg" 
                     value="Register"/> &nbsp; <!-- Click to submit for registration -->
              <input type="reset"
                      value="Clear Form"/> <!-- Click to clear the form -->       
            </p>             
          </form> 
        </div>    <!-- div container for form content ends here -->  
      </div>
    </section>
    
    <!-- Semnatic Element Footer for footer menu for the website, 
        the copyright display, and last modified date and time -->
    <footer>
      <div id="displaymsg3">
            Salon Timings: 10 a.m. till 5 p.m. Mondays closed.
      </div>
      <div id="footer_menu">
        <ul id="footer_navigation">      
            <li><a title="Page Not Ready" href="#">CAREERS</a></li>
            <li><a title="Page Not Ready" href="#">GALLERY</a></li>
            <li><a title="Page Not Ready" href="#">PRIVACY POLICY</a></li>
            <li class="current_page"><a title="Contact Us" href="/contactus">CONTACT</a></li>   
            <!-- <li><a title="Marking Page" href="Marking/marks.html">MARKS</a></li>                             -->
        </ul>
      </div>
      <div id="site_details">
        <div id="copy_right">
            &copy; 2020 AYN Technologies Ltd. All Rights Reserved. &nbsp;
        </div> 
        <div id="last_modified">
          <script>
            document.scripts[document.scripts.length-1].parentNode.innerHTML = "Last Modified: " + document.lastModified;
          </script>
        </div>
      </div> 
    </footer>
    
  </div> <!-- A Div to containing all elements ends here -->
</body>
</html>