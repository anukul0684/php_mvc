<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    //
    /**
     * home() function to call the home page
     * @return index.blade.php
     */
    public function home()
    {
        return view('index');
    }

    /** 
     * services() function to call the services page
     * @return services.blade.php
     */
    public function services()
    {
        return view('services');
    }

    /**
     * aboutus() function to call the about us page
     * @return aboutus.blade.php
     */
    public function aboutus()
    {
        return view('aboutus');
    }

    /**
     * contactus() function to call the contact us page
     * @return contactus.blade.php
     */
    public function contactus()
    {
        return view('contactus');
    }

    /**
     * courses() function to call the courses page
     * @return courses.blade.php
     */
    public function courses()
    {
        return view('courses');
    }

    /**
     * servicesathome() function to call the services at home page
     * @return servicesathome.blade.php
     */
    public function servicesathome()
    {
        return view('servicesathome');
    }

    /**
     * seminars() function to call the seminars page
     * @return seminars.blade.php
     */
    public function seminars()
    {
        return view('seminars');
    }

    /**
     * products() function to call the products page
     * @return products.blade.php
     */
    public function products()
    {
        return view('products');
    }

    /**
     * specials() function to call the specials page
     * @return specials.blade.php
     */
    public function specials()
    {
        return view('specials');
    }
}
