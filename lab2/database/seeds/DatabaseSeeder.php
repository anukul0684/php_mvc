<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(seed_movies_table::class);
        $this->call(UsersSeeder::class);
        factory(App\User::class,20)->create();
    }
}
