<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class seed_movies_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('movies')->insert(
            [
                'title' => 'F.A.L.T.U',
                'summary' => 'Unable to get accepted, four academically-challenged 
                                students decide to open their own college.',
                'description' => "A group of friends, Ritesh (Jackky), Nanj (Angad), and Puja (Puja), 
                                    all receive extremely bad marks in their exams. One of their close friends, 
                                    Vishnu (Chandan) has passed with top marks under the pressure of his father 
                                    and has enrolled into the top high school of India. To make their parents 
                                    happy and proud, the four friends create a fake university titled 
                                    'Fakirchand and Lakirchand Trust University' (or F.A.L.T.U) with the help 
                                    of Ritesh's childhood friend Google (Arshad Warsi). Things take a turn for 
                                    the worse when the parents would like to see F.A.L.T.U. To make things go right, 
                                    Ritesh and Google hire someone, Bajirao (Riteish), to act as the principal for 
                                    one day. However, after the parents' visit, a bunch of kids apply for 
                                    F.A.L.T.U thinking it is a real university. Unable to send them back, 
                                    the trio, along with Vishnu, Google and Bajirao, turn F.A.L.T.U into an 
                                    official trust university. Soon enough, the government files a case against 
                                    every student/member of F.A.L.T.U for creating a fake college. Now the group 
                                    of friends must fight for their rights, and keep F.A.L.T.U as a university 
                                    to give the kids an education.",
                'director' => "Remo D'Souza",
                'running_time' => 127,
                'image' => 'faltu.jpg',
                'studio' => 'Puja Entertainment (India) Ltd.',
                'genre' => 'Comedy, Drama',
                'language' => 'Hindi',
                'release_date' => '2011-04-01',
                'parent_guide' => 'PG',
                'created_at' => Carbon::now()                
            ]
        );

        DB::table('movies')->insert(
            [
                'title' => 'Band Baaja Baaraat',
                'summary' => "Shruti and Bittoo become partners in their very own 
                              'Wedding planning ka bijness' in Delhi and in the process 
                               discover friendship, love and one another.",
                'description' => 'Shruti (Anushka Sharma) is a 20-something no-nonsense girl 
                                from a middle class Delhi household. Focused and determined with 
                                preplanned ambitions, her goals in life are well laid out by the time 
                                she reaches her final year of college. Bittoo (Ranveer Singh), on the 
                                other hand, has no real aim in life. As a final year college student of 
                                Delhi University, he whiles away his life having fun with his buddies, barely 
                                scraping through his exams. A chance and inopportune meeting (or as you would call it, fate) 
                                brings the two of them together on a tumultuous journey.',
                'director' => 'Maneesh Sharma',
                'running_time' => 140,
                'image' => 'bbb.jpg',
                'studio' => 'Yash Raj Films',
                'genre' => 'Comedy, Drama, Romance',
                'language' => 'Hindi',
                'release_date' => '2010-12-10',
                'parent_guide' => '14A',
                'created_at' => Carbon::now()->addSeconds(5)                
            ]
        );

        DB::table('movies')->insert(
            [
                'title' => 'Rocket Singh: Salesman of the Year',
                'summary' => "A story of a fresh graduate trying to find a balance between 
                                the maddening demands of the 'professional' way, and the way of his heart.",
                'description' => "Harpreet Singh Bedi (Ranbir Kapoor) has just graduated and
                                 decides to five into the world of sales for an adventurous career. 
                                 But soon his idea of success begins to clash. It's the story of a 
                                 fresh graduate trying to find a balance between the maddening demands 
                                 of the 'professional' way, and the way of his heart - and stumbling upon a 
                                 crazy way which turned his world upside down and his career right side up.",
                'director' => 'Shimit Amin',
                'running_time' => 156,
                'image' => 'rocket_singh.jpg',
                'studio' => 'Yash Raj Films',
                'genre' => 'Comedy, Drama',
                'language' => 'Hindi',
                'release_date' => '2009-12-11',
                'parent_guide' => 'PG',
                'created_at' => Carbon::now()->addSeconds(10)                
            ]
        );

        DB::table('movies')->insert(
            [
                'title' => 'Chef',
                'summary' => 'A head chef quits his restaurant job and buys a 
                                food truck in an effort to reclaim his creative promise, 
                                while piecing back together his estranged family.',
                'description' => "Carl Casper is an acclaimed chef with a family life that 
                                seems as decaying as his artistic freedom. Those frustrations 
                                boil over into a raucous viral-videoed public confrontation against 
                                a restaurant critic who panned his cooking of food that his boss ordered 
                                him to make against his instincts. Now with his career ruined, Carl's ex-wife 
                                offers an unorthodox solution in Miami: refit an old food truck to offer quality 
                                cooking on his own terms. Now with his young son, Percy, and old colleague, Martin, 
                                helping, Carl takes a working trip across America with that truck to rediscover 
                                his gastronomic passion. With Percy's tech savvy and Martin's enthusiasm, 
                                Carl finds that he is creating a traveling sensation on the way home. In doing so, 
                                Carl discovers he is serving up more than simply food, but also a deeper connection 
                                with his life and his family that is truly delicious in its own way.",
                'director' => 'Jon Favreau',
                'running_time' => 114,
                'image' => 'chef.jpg',
                'studio' => 'Fairview Entertainment',
                'genre' => 'Adventure, Comedy, Drama',
                'language' => 'English',
                'release_date' => '2014-05-30',
                'parent_guide' => '14A',
                'created_at' => Carbon::now()->addSeconds(15)                
            ]
        );

        DB::table('movies')->insert(
            [
                'title' => 'Terminator 2: Judgment Day',
                'summary' => 'A cyborg, identical to the one who failed to kill 
                            Sarah Connor, must now protect her teenage son, John Connor, 
                            from a more advanced and powerful cyborg.',
                'description' => "Over 10 years have passed since the first machine called 
                                    The Terminator tried to kill Sarah Connor and her unborn son, John. 
                                    The man who will become the future leader of the human resistance against 
                                    the Machines is now a healthy young boy. However, another Terminator, 
                                    called the T-1000, is sent back through time by the supercomputer Skynet. 
                                    This new Terminator is more advanced and more powerful than its predecessor 
                                    and its mission is to kill John Connor when he's still a child. However, in 
                                    the Internet and John do not have to face the threat of the T-1000 alone. 
                                    Another Terminator (identical to the same model that tried and failed to kill 
                                    Sarah Connor in 1984) is also sent back through time to protect them. Now, the 
                                    battle for tomorrow has begun.",
                'director' => 'James Cameron',
                'running_time' => 137,
                'image' => 'terminator2.jpg',
                'studio' => 'TriStar Pictures',
                'genre' => 'Action, Sci-Fi',
                'language' => 'English',
                'release_date' => '1991-07-01',
                'parent_guide' => '18A',
                'created_at' => Carbon::now()->addSeconds(20)                
            ]
        );

        DB::table('movies')->insert(
            [
                'title' => 'Battleship',
                'summary' => 'A fleet of ships is forced to do battle with an armada of 
                                unknown origins in order to discover and thwart their destructive goals.',
                'description' => "An international fleet of naval warships encounters an alien armada while
                                 on a Naval war games exercise and faces the biggest threat mankind has ever 
                                 faced. An intense battle is fought on sea, land and air. If they lose, 
                                 the world could face a major extinction event and an alien invasion. 
                                 Will humans win this alien war, what are the aliens doing here, and what do they want?",
                'director' => 'Peter Berg',
                'running_time' => 131,
                'image' => 'battleship.jpg',
                'studio' => 'Hasbro Studios',
                'genre' => 'Action, Adventure, Sci-Fi',
                'language' => 'English',
                'release_date' => '2012-05-18',
                'parent_guide' => 'PG',
                'created_at' => Carbon::now()->addSeconds(25)                
            ]
        );

        DB::table('movies')->insert(
            [
                'title' => 'Ratatouille',
                'summary' => 'A rat who can cook makes an unusual alliance with a 
                            young kitchen worker at a famous restaurant.',
                'description' => "A rat named Remy dreams of becoming a great French chef 
                                despite his family's wishes and the obvious problem of being 
                                a rat in a decidedly rodent-phobic profession. When fate places 
                                Remy in the sewers of Paris, he finds himself ideally situated beneath 
                                a restaurant made famous by his culinary hero, Auguste Gusteau. Despite 
                                the apparent dangers of being an unlikely, and certainly unwanted, visitor 
                                in the kitchen of a fine French restaurant, Remy's passion for cooking soon 
                                sets into motion a hilarious and exciting rat race that turns the culinary 
                                world of Paris upside down.",
                'director' => 'Brad Bird, Jan Pinkava (co-director)',
                'running_time' => 111,
                'image' => 'ratatouille.jpg',
                'studio' => 'Pixar Animation Studios',
                'genre' => 'Animation, Adventure, Comedy',
                'language' => 'English',
                'release_date' => '2007-06-29',
                'parent_guide' => 'G',
                'created_at' => Carbon::now()->addSeconds(30)                
            ]
        );

        DB::table('movies')->insert(
            [
                'title' => 'Megamind',
                'summary' => 'The supervillain Megamind finally defeats his nemesis, the superhero Metro Man. 
                                But without a hero, he loses all purpose and must find new meaning to his life.',
                'description' => "After super-villain Megamind (Ferrell) kills his good-guy nemesis, Metro Man (Pitt), 
                                    he becomes bored since there is no one left to fight. He creates a new foe, 
                                    Tighten (Hill), who, instead of using his powers for good, sets out to destroy the world, 
                                    positioning Megamind to save the day for the first time in his life.",
                'director' => 'Tom McGrath',
                'running_time' => 96,
                'image' => 'megamind.jpg',
                'studio' => 'Paramount Pictures, DreamWorks Animation',
                'genre' => ' Animation, Action, Comedy',
                'language' => 'English',
                'release_date' => '2010-11-05',
                'parent_guide' => 'PG',
                'created_at' => Carbon::now()->addSeconds(35)                
            ]
        );       

        DB::table('movies')->insert(
            [
                'title' => "Harry Potter and the Sorcerer's Stone",
                'summary' => 'An orphaned boy enrolls in a school of wizardry, 
                                where he learns the truth about himself, his family and the terrible 
                                evil that haunts the magical world.',
                'description' => "This is the tale of Harry Potter (Daniel Radcliffe), an ordinary 
                                    eleven-year-old boy serving as a sort of slave for his aunt and uncle 
                                    who learns that he is actually a wizard and has been invited to attend 
                                    the Hogwarts School for Witchcraft and Wizardry. Harry is snatched away 
                                    from his mundane existence by Rubeus Hagrid (Robbie Coltrane), the groundskeeper 
                                    for Hogwarts, and quickly thrown into a world completely foreign to both him and 
                                    the viewer. Famous for an incident that happened at his birth, Harry makes friends 
                                    easily at his new school. He soon finds, however, that the wizarding world is far more 
                                    dangerous for him than he would have imagined, and he quickly learns that not all wizards 
                                    are ones to be trusted.",
                'director' => 'Chris Columbus',
                'running_time' => 152,
                'image' => 'harrypotter1.jpg',
                'studio' => 'Warner Bros. Pictures',
                'genre' => 'Adventure, Family, Fantasy',
                'language' => 'English',
                'release_date' => '2001-11-16',
                'parent_guide' => 'PG',
                'created_at' => Carbon::now()->addSeconds(45)                
            ]
        );
        
    }
}
