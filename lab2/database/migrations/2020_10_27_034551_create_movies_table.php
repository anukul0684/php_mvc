<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->id();

            //title VARCHAR(255)
            $table->string('title',255);

            //summary TEXT
            $table->text('summary');

            //description LONGTEXT
            $table->longText('description');

            //director TEXT
            $table->text('director');

            //running_time INTEGER
            $table->integer('running_time');

            //image VARCHAR(255)
            $table->string('image',255);

            //studio VARCHAR(255)
            $table->string('studio',255);

            //genre VARCHAR(255)
            $table->string('genre',255);

            //actor_id INTEGER
            //$table->foreignId('actor_id');

            //genre_id INTEGER
            //$table->foreignId('genre_id');

            //category_id INTEGER
            //$table->foreignId('category_id');

            //language
            $table->text('language');

            //release_date
            $table->date('release_date');

            //Parents Guide
            $table->enum('parent_guide',['G','PG','14A','18A','R','A']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
