@extends('layouts/movie')

@section('navbar')
    @include('partials/navbar')
@stop()



@section('content')
    <div class="col-sm-6">
        <div class="card mt-4">
            <img class="card-img-top img-fluid" 
                    src="/images/{{ substr($movie->image, 0, strpos($movie->image,'.')) . 'B.jpg' }}" 
                    alt="{{ $movie->title }}">            
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col-lg-6 -->

    <div class="col-sm-6">
        <div class="card card-outline-secondary my-4">
            <div class="card-header">
                <h3 class="card-title"> {{$movie->title}} </h3>
                <h4>Director&#58; {{ $movie->director }}</h4>
                <p class="card-text">{{ $movie->summary }}</p>
                <p class="card-text">Duration&#58; {{ $movie->running_time }} minutes</p>
                <span class="text-warning">&#9733; &#9733; &#9733; &#9733; &#9734;</span>
                4.0 stars
            </div>
            <div class="card-body">
                <p>{{ $movie->description }}</p>
                <p>Studio&#58; {{ $movie->studio }}</p>
                <p>Genre&#58; {{ $movie->genre }}</p>
                <p>Language&#58; {{ $movie->language }}</p>
                <p>Parental Guidance&#58; {{ $movie->parent_guide }}</p>
                <small class="text-muted">Released on&#58; {{$movie->release_date}}</small>
                <hr>                
                <a href="#" class="btn btn-success">Leave a Review</a>&nbsp;
                <a href="/movies" class="btn btn-primary">Go Back</a>
            </div>
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col-lg-6 -->
@stop()