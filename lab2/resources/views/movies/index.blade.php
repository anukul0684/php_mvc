@extends('layouts/movie')

@section('navbar')
    @include('partials/navbar')
@stop()

@section('sidebar')
    @include('partials/sidebar')
@stop()

@section('content')
    <div class="col-lg-9">

        <div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
            <ol class="carousel-indicators">
                
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                @for($i=1;$i<count($movies);$i++)                    
                    <li data-target="#carouselExampleIndicators" data-slide-to="{{ $i }}"></li>
                @endfor
                
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <a href="/movies/{{ $movies[0]->id }}" title="{{ $movies[0]->title }}">
                        <img class="d-block img-fluid" 
                        src="/images/{{substr($movies[0]->image, 0, strpos($movies[0]->image,'.')) . 'W.jpg'}}" 
                        alt="{{ $movies[0]->title }}">
                        </a>
                </div>
                @foreach($movies as $movie)     
                    @if($loop->first) @continue @endif               
                    <div class="carousel-item">
                        <a href="/movies/{{ $movie->id }}" title="{{ $movie->title }}">
                            <img class="d-block img-fluid" 
                            src="/images/{{ substr($movie->image, 0, strpos($movie->image,'.')) . 'W.jpg' }}" 
                            alt="{{ $movie->title }}">
                        </a>
                    </div>
                @endforeach
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

        <div class="row">

            @foreach($movies as $movie)
            <div class="col-lg-4 col-md-6 mb-4">
                <div class="card h-100">
                <a href="/movies/{{ $movie->id }}"><img class="card-img-top" src="/images/{{ $movie->image }}" alt=""></a>
                <div class="card-body">
                    <h4 class="card-title">
                        <a href="/movies/{{ $movie->id }}">{{ $movie->title }}</a>
                    </h4>
                    <h5>Director&#58; {{ $movie->director }}</h5>
                    <p class="card-text">{{ $movie->summary }}</p>
                    <p class="card-text">{{ $movie->running_time }} minutes</p>
                    <p class="card-text">{{ $movie->genre }}</p>
                </div>
                <div class="card-footer">
                    <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
                </div>
                </div>
            </div>
            @endforeach
            <!-- Pagination -->
            <div class="pagination justify-content-center mb-4">            
                {{ $movies->links() }}
            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.col-lg-9 -->
@stop()