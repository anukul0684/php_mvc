@if(session('success'))
    <div style="width:100%;" class="alert alert-success mt-4 mb-0">
        {{ session('success') }}
    </div>
@endif
@if(session('error'))
<div style="width:100%;" class="alert alert-danger mt-4 mb-0">
        {{ session('error') }}
    </div>
@endif