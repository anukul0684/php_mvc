<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Bootstrap core CSS -->
  <link href="/css/app.css" rel="stylesheet">  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
  <style>
        #wrapper {
            background: #ccc;
            overflow: hidden;
            transition: height 200ms;
            height: 0; /* <-- set this */
        }
  </style>
</head>

<body>
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v12.0&appId=176770547996787&autoLogAppEvents=1" nonce="omCaDVmx"></script>
    @yield('navbar')

    <!-- Page Content -->
    <div class="container">

        <div class="row">
            <h1></h1>
            <h2></h2>
            <h3></h3>
            @yield('sidebar')

            @yield('content')

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">
        <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Illuminix 2020</p>
        </div>
        <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <!-- <script src="/js/app.js"></script> -->
    <script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap.bundle.min.js"></script>
    <script>
        $(function() {
            var b = $("#button");
            var w = $("#wrapper");
            var l = $("#list");

            // w.height(l.outerHeight(true)); REMOVE THIS 

            b.click(function() {

                if (w.hasClass('open')) {
                w.removeClass('open');
                w.height(0);
                } else {
                w.addClass('open');
                w.height(l.outerHeight(true));
                }

            });
        });
    </script>
</body>

</html>
