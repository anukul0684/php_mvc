@extends('layouts.app')

@section('content')
<div class="container mb-5 mt-5">
    <div class="row justify-content-center mb-5 mt-5">
        <div class="col-md-8 mb-5 mt-5">
            <div class="card mb-5 mt-5">
                <div class="card-header mb-4 mt-4">{{ __('Admin Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection