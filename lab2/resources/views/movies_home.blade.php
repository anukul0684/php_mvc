@extends('layouts/movie')

@section('navbar')
    @include('partials/navbar')
@stop()

@section('content')
     <!-- Jumbotron Header -->
     <header class="jumbotron my-4">
      <h1 class="display-3">A Warm Welcome!</h1>
      <p class="lead">
        Watching a movie is like reading a book. And if the book is interesting we encourage
        you to watch it over and over again. We welcome you to a place of wonderful collection in movies.
        Here you will find some ever-green movies, and the ones that has a greater impact on to our lives.
        Each movie carries a message, something to learn from and illuminate the innerself with positive 
        and hopeful energies. Please click on to the below link or on the Movies tab to take you to a 
        different world.
      </p>
      <a href="/movies" class="btn btn-primary btn-lg">Let's Go!</a>
    </header>
    <section id="services" class="bg-light">
        <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto">
              <h2>Illuminix feels</h2>
              <blockquote class="lead">
                <cite>
                  We have three roles here on earth: to learn, to love, and to live. 
                  When we stop learning, we start to stagnate and die. When we stop loving, 
                  we lose our sense of purpose and become self-centered. When we limit our living, 
                  we deny the world the benefits of our talents.
                </cite>
              </blockquote>
            </div>
        </div>
        </div>
    </section>
@stop()