<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route to the Home page when user visits the website
Route::get('/', function () {
    $title="Home Page";
    return view('movies_home',compact('title'));
});

//Route to the About Us page when user clicks About on the website
Route::get('/about', function () {
    $title="About Us";
    return view('about',compact('title'));
});

//Route to the Contact page when user clicks Contact on the website
Route::get('/contact', function () {
    $title="Contact";
    return view('contact',compact('title'));
});

//Route to the list view of Movies when user clicks Movies or Let's Go or Back
Route::get('/movies','MoviesController@index');

//Route to the detail view of the selected or clicked movie on its title or image
//using the id of the movies table
Route::get('/movies/{id}','MoviesController@show');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
