<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;
use \Auth;

class MoviesController extends Controller
{
    //
    /**
     * index() function to get the list view for all movies 
     * to display on the Movies tab on the website navbar
     * @return movies/index.blade.php with variables title and movies
     */
    public function index()
    {
        $movies = Movie::latest()->simplePaginate(3);
        //$movies = Movie::latest()->simplePaginate(3);
        $title = "All Movies";
        return view('movies/index',compact('title','movies'));
    }

    /**
     * show($id) function to get a record of particular movie id
     * @param int id (id of the movies table)
     * @return movies/show.blade.php with variables title and movie
     */
    public function show($id)
    {
        $movie = Movie::find($id);
        $title = $movie->title;
        return view('movies/show',compact('title','movie'));
    }
}
