<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Post;
use Illuminate\Support\Str;

class Category extends Model
{
    //
    protected $fillable = ['name','slug','category_image'];   
    /**
     * posts() relationship between category and posts
     * @return Collection of posts for a category
     */
    public function posts()
    {
        return $this->hasMany(Post::class);
    }
}
