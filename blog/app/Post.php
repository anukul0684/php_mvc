<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;

class Post extends Model
{
    //
    /**
     * category() relationship between Posts and Category
     * @return Category of a post
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * comments() relationship between Posts and Comments
     * @return Collection of comments of a Post
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}
