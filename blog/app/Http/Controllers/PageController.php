<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Mail;
class PageController extends Controller
{
    //
    public function contact()
    {
        $title="Contact Us";
        return view('contact',compact('title'));
    }

    public function contact_send(Request $request)
    {
        $valid = $request->validate([
            'name'=>'bail|required|string|max:255',
            'email' => 'bail|required|email',
            'subject'=>'bail|required|string|max:255',
            'text' => 'bail|required|string'
        ]);
        $to_email = "kulshresthanu9@gmail.com";
        $to_name = "Webmaster, WDD7 blog";

        Mail::send('emails/contact',$valid,function($message) use($valid,$to_email,$to_name){

            $message->to($to_email,$to_name)->subject('Message from Blog Contact Form');
            $message->from($valid['email'],$valid['subject']);
        });

        session()->flash('success','Your message is been sent successuflly');
        return redirect()->back();
    }
}
