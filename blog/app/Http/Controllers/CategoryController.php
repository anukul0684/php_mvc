<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;

class CategoryController extends Controller
{
    //
    public function show(Category $cat)
    {
        //$post = Post::where('slug',$slug)->first();
        //$posts = Post::latest()->simplePaginate(5); //previous and next 
        //$posts = Post::latest()->get($cat->id);

        $posts=$cat->posts()->latest()->simplePaginate(2);
        $cats = Category::orderBy('name','ASC')
                        ->with('posts')
                        ->get();        
        $title = 'Categories';
        return view('category/show', compact('cats','title','posts','cat'));      
    }
}
