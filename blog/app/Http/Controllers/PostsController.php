<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;
use App\Comment;
use \Auth;

class PostsController extends Controller
{
    //
    public function index()
    {
        //$posts = Post::all();
        // $posts = Post::latest()->get();
        //$posts = Post::latest()->paginate(5);
        $posts = Post::latest()->simplePaginate(2); //previous and next 
        $posts = Post::latest()
                        ->with('category')
                        ->simplePaginate(2);
        $cats = Category::orderBy('name','ASC')
                            ->with('posts')
                            ->get();
        $title = 'Latest Posts';
        return view('blog/index', compact('posts','title','cats'));
    }

    //public function show($slug)
    public function show(Post $post)
    {
        //$post = Post::where('slug',$slug)->first(); //method 1 when slug is passed
        $cat = Category::find($post->category_id); //method 2 when data binding Post variable is passed
        $cats = Category::orderBy('name','ASC') //method 1
                            ->with('posts')
                            ->get();
        // $cats = Category::orderBy('name','ASC') //method 2
        //                  ->withCount('posts')
        //                  ->get();
        $comments = $post->comments()->with('user')->get();
        //dd($comments);
        $title = $post->title;
        return view('blog/show',compact('title','post','cat','cats','comments'));
    }


    public function store_comment(Request $request, Post $post)
    {
        //dd($request->input(),$post,\Auth::user());
        //post id
        //user id
        //body comment
        //created_at

        $valid = $request->validate(
            [
                'body' => 'bail|required|string'
            ]
        );

        //dd($valid);
        //create a comment

        //Method 1
        // $comment = new Comment(
        //     [
        //         'user_id' => Auth::user()->id,
        //         'post_id' => $post->id,
        //         'body' => $valid['body']
        //     ]
        // );
        //$comment->save();

        //Method 2
        // $comment = Comment::create(
        //     [
        //         'user_id' => Auth::user()->id,
        //         'post_id' => $post->id,
        //         'body' => $valid['body']
        //     ]
        // );

        //Method 3
        //$comment = Comment::create($valid);
        
        //Method 4
        $comment = new Comment();
        $comment->user_id = Auth::user()->id;
        $comment->post_id = $post->id;
        $comment->body = $valid['body'];
        $comment->save();
        
        session()->flash('success','Your comment was added sucessfully');

        //redirect to detail page of post
        return redirect()->back();

    }
}
