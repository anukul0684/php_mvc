<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use Illuminate\Support\Str;

class CategoriesController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $cats = Category::all();
        $cats = Category::withCount('posts')
                            ->orderBy('name')
                            ->get();

        return view('admin/categories/index',compact('cats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin/categories/create');
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = $request->validate(
            [
                'name' => 'bail|required|max:255',
                'category_image' => 'nullable|image'
            ]
        );
        //dd($valid);
        $orig_name = $request->file('category_image')->getClientOriginalName();
        $new_name = Str::slug(now(),'-') . '_' . $orig_name;
        //dd($new_name);

        $request->file('category_image')->storeAs('public/icons',$new_name);  
             

        $valid['slug'] = Str::slug($valid['name'],'-');

        $valid['category_image'] = $new_name;
        
        //$valid['category_image'] = 'category.jpg';

        $cat = Category::create($valid);

        if($cat) {
            session()->flash('success','Category was added successfully');            
        } else {
            session()->flash('error','Issue createing category. Try Again.');
        }
        $title = 'Add Category';
        return redirect('/admin/categories');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $cat)//method1
    {
        //Get the category 
        //$cat = Category::find($id);//method 2
        // $cat = Category::where('id',$id)->first(); //method3
        //Load the edit form
        $title = 'Edit Cateogry';
        return view('admin/categories/edit',compact('cat','title'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $cat)
    {
        //
        //dd($request->input());

        $valid = $request->validate([
            'id' => 'bail|required|integer',
            'name' => 'bail|required|string',
            'category_image' => 'nullable|image'
        ]);

        $orig_name = $request->file('category_image')->getClientOriginalName();
        $new_name = Str::slug(now(),'-') . '_' . $orig_name;
        //dd($new_name);

        $request->file('category_image')->storeAs('public/icons',$new_name);  

        $valid['category_image'] = $new_name;
        //$cat = Category::find($valid['id]);
        if($cat->update($valid)) {
            session()->flash('success','The category was successfully updated');
        } else {
            session()->flash('error','Some issue occurred in updating category. Please try again');
        }
        
        
        return redirect('/admin/categories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Category $cat)
    {
        //
        $valid = $request->validate([
            'id' => 'bail|required|integer'
        ]);
            
        //$cat = Category::find($valid['id']);
            $cat->delete();
        session()->flash('success', 'The category was deleted');
        return redirect()->back();
    }
}
