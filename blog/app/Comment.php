<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //
    protected $fillable = ['user_id','post_id','body'];
    /**
     * post() relationship between Comment and Post
     * @return post that got the comment
     */
    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    /** 
     * user() relationship between Comment and User
     * @return user that wrote the comment
    */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
