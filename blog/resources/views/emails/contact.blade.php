<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>From the Blog Contact Form</title>
</head>
<body>

    <h1>You received a message from the Blog Contact Form</h1>
    <ul>
        <li><strong>From</strong>{{$name}} -- {{$email}}</li>
        <li><strong>Subject</strong>{{$subject}}</li>
        <li><strong>Message</strong>{{$text}}</li>
    </ul>    
</body>
</html>