@extends('layouts/blog')

@section('content')
    @include('partials/flash')
    <div class="container">        
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header mb-4 mt-4">{{ __('Contact Form') }}</div>

                    <div class="card-body">
                    <form class="form" action="/contact" method="post">

                        @csrf

                        <div class="form-group">
                            <label for='name'>Your Name</label>
                            <input class="form-control" type="text" name="name" value="{{ old('name') }}" />
                            @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for='email'>Your Email Address</label>
                            <input class="form-control" type="text" name="email" value="{{ old('email') }}" />
                            @error('email')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for='subject'>Subject</label>
                            <input class="form-control" type="text" name="subject" value="{{ old('subject') }}" />
                            @error('subject')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="">
                            <label for="text">Your Message</label>
                            <textarea class="form-control" name="text">{{ old('text')  }}</textarea>
                            @error('text')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="py-4 px-4">
                            <button class="btn btn-primary" type="submit">Send</button>
                        </div>


                        </form>
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        {{ __('You are logged in!') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop()