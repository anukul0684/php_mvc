@extends('layouts/blog')

@section('content')
    <!-- Post Content Column -->
    <div class="col-lg-8">

        <!-- Title -->
        <h1 class="mt-4">{{ $post->title }}</h1>

        <!-- Author --> <!-- Date/Time -->
        <p class="lead">
        Posted in
        <a href="/category/{{ $post->category->name }}">{{ $cat->name }}</a> on {{ $post->created_at }}
        </p>

        <hr>      
        

        <!-- Preview Image -->
        <img class="img-fluid rounded" src="/images/{{ $post->featured_image }}" alt="{{$post->title}}">

        <hr>

        <!-- Post Content -->
        <p class="lead">{{ $post->summary }}</p>

        <p>{!! nl2br($post->body) !!}</p>

        
        <blockquote class="blockquote">
            <footer class="blockquote-footer">Viewed
                <cite title="Source Title">{{$post->num_views}}</cite> 
            </footer>
            <hr>
            <a href="/blog" class="btn btn-primary">&larr; Back</a>
        </blockquote>
        
        <hr id="leave_comment">
       
        <!-- Comments Form -->
        <div class="card my-4">
            <h5 class="card-header">Leave a Comment:</h5>
            <div class="card-body">
                
                @guest
                    <div class="alert alert-info">
                        <p class="mt-3">please &nbsp;<a class="btn btn-primary" href="/login">login</a>&nbsp; or 
                        &nbsp;<a class="btn btn-success" href="/register">register</a>&nbsp;
                        to leave a comment.</p>
                    </div>
                @else
                @include('partials/flash')
                <form action="/blog/{{ $post->slug }}#leave_comment" method="post" novalidate>
                    @csrf
                    <div class="form-group">
                        @error('body')
                            <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                        <textarea name="body" class="form-control" rows="3"></textarea>
                        
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
                @endguest
            </div>
        </div>
        
        <!-- Single Comment -->
        <!-- <div class="media mb-4">
            <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
            <div class="media-body">
                <h5 class="mt-0">Commenter Name</h5>
                Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
            </div>
        </div> -->

        <!-- Comment with nested comments -->
        @foreach($comments as $comment)
        <div class="media mb-4 col">            
            <div class="media-body">        
                    @if($loop->first)              
                        <img class="d-flex mr-3 rounded-circle" 
                        src="https://gravatar.com/avatar/md5({{$comment->user->email}})?s=50" 
                        alt="{{ $comment->user->name }}" title="{{$comment->user->email}}">{{ $comment->user->name }}   
                    @endif
                    @foreach($comments as $comment)                 
                    <div class="media mt-4">
                        <div class="media-body">
                            {{ $comment->created_at }}
                            <h5 class="mt-0">{{ $comment->body }}</h5>
                        </div>
                    </div> 
                    @endforeach 
            </div>            
        </div>
        @endforeach

    </div>
@stop()

@section('sidebar')
    @include('partials/sidebar')
@stop()