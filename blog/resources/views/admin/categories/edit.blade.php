@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header mb-4 mt-4">{{ __('Admin Dashboard') }}</div>

                    <div class="card-body">
                        <h2>{{ $title }}</h2>
                        <p><a href="/admin/categories" class="btn btn-warning">Back</a></p>
                        <form class="form" action="/admin/categories/edit/{{$cat->id}}" enctype="multipart/form-data" method="post" novalidate>
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <input type="hidden" name="id" value="{{old('id',$cat->id)}}" />
                                <label for="name">Category Name</label>
                                <input class="form-control" type="text" name="name" value="{{old('name', $cat->name)}}"/>
                                @error('name')
                                    <div class="alert alert-danger">
                                        {{message}}
                                    </div>
                                @enderror
                                </div>
                                <div class="form-group">
                                @if($cat->category_image)
                                    <img src="/storage/icons/{{$cat->category_image}}" alt="" />
                                @endif
                                    <label for="category_image">Icon</label>
                                    <input type="file" name="category_image" />
                                    @error('icon')
                                        <div class="alert alert-danger">
                                            {{ message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
