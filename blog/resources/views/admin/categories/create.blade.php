@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header mb-4 mt-4">{{ __('Admin Dashboard') }}</div>

                <div class="card-body">
                    <h2>Create New Category</h2>
                    <p><a href="/admin/categories" class="btn btn-warning">Back</a></p>
                    <form class="form" action="/admin/categories" method="post" enctype="multipart/form-data" novalidate>
                        @csrf
                        <div class="form-group">
                            <label for="name">Category Name</label>
                            <input class="form-control" type="text" name="name" value="{{old('name')}}"/>
                            @error('name')
                                <div class="alert alert-danger">
                                    {{ message }}
                                </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="category_image">Icon</label>
                            <input type="file" name="category_image" />
                            @error('icon')
                                <div class="alert alert-danger">
                                    {{ message }}
                                </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
