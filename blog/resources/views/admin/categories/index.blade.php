@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header mb-4 mt-4">{{ __('Admin Dashboard') }}</div>
                @include('partials/flash')
                <div class="card-body">
                    <a href="/admin/categories/create" class="btn btn-success mb-4">Create Category</a>
                    <table class="col">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Category</th>
                            <th>Number of Posts</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($cats as $cat)
                        <tr>
                            <td>{{$cat->id}}</td>
                            <td>{{$cat->name}}</td>
                            <td>{{$cat->posts_count}}</td>
                            <td>
                                <a class="btn btn-primary btn-sm" href="/admin/categories/edit/{{ $cat->id }}">Edit</a>
                                &nbsp;
                                <form class="form form-inline" action="/admin/categories/delete/{{$cat->id}}" method="post">
                                    @csrf 
                                    @method('DELETE')
                                    <input type="hidden" name="id" value="{{$cat->id}}" />
                                    <button class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
