<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();

            //https://laravel.com/docs/8.x/migrations#creating-columns
            
            //title //VARCHAR(255)
            $table->string('title',255);

            //slug VARCHAR(255)
            $table->string('slug',255);

            //summary //TEXT
            $table->text('summary');

            //num_views //INTEGER
            $table->integer('num_views')->default(0);

            //body //LONGTEXT
            $table->longText('body');
            
            //featured_image //VARHCAR(255)
            $table->string('featured_image')->default(0);

            //user_id //INTEGER
            //$table->foreignId('user_id');

            //category_id //INTEGER 
            //$table->foreign('category_id')
                        // ->references('id')
                        // ->on('Categories')
                        // ->onDeleted('set null');
            // $table->foreignId('category_id')
            //         ->nullable()
            //         ->constrained()
            //         ->onDelete('set null');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
