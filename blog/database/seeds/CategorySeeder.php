<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Str;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('categories')->insert(
            [
                'name' => 'general',
                'category_image' => 'category.jpg',
                'slug' => Str::slug('name','-'),
                'created_at' => Carbon::now()->addSeconds(0)
            ]
        );

        DB::table('categories')->insert(
            [
                'name' => 'programming',
                'category_image' => 'category.jpg',
                'slug' => Str::slug('name','-'),
                'created_at' => Carbon::now()->addSeconds(5)
            ]
        );

        DB::table('categories')->insert(
            [
                'name' => 'Design',
                'category_image' => 'category.jpg',
                'slug' => Str::slug('name','-'),
                'created_at' => Carbon::now()->addSeconds(10)
            ]
        );

        DB::table('categories')->insert(
            [
                'name' => 'html',
                'category_image' => 'category.jpg',
                'slug' => Str::slug('name','-'),
                'created_at' => Carbon::now()->addSeconds(15)
            ]
        );

        DB::table('categories')->insert(
            [
                'name' => 'css',
                'category_image' => 'category.jpg',
                'slug' => Str::slug('name','-'),
                'created_at' => Carbon::now()->addSeconds(20)
            ]
        );

        DB::table('categories')->insert(
            [
                'name' => 'xml',
                'category_image' => 'category.jpg',
                'slug' => Str::slug('name','-'),
                'created_at' => Carbon::now()->addSeconds(25)
            ]
        );

        DB::table('categories')->insert(
            [
                'name' => 'javascript',
                'category_image' => 'category.jpg',
                'slug' => Str::slug('name','-'),
                'created_at' => Carbon::now()->addSeconds(30)
            ]
        );

        DB::table('categories')->insert(
            [
                'name' => 'php',
                'category_image' => 'category.jpg',
                'slug' => Str::slug('name','-'),
                'created_at' => Carbon::now()->addSeconds(35)
            ]
        );

        DB::table('categories')->insert(
            [
                'name' => 'mysql',
                'category_image' => 'category.jpg',
                'slug' => Str::slug('name','-'),
                'created_at' => Carbon::now()->addSeconds(40)
            ]
        );

        DB::table('categories')->insert(
            [
                'name' => 'vue',
                'category_image' => 'category.jpg',
                'slug' => Str::slug('name','-'),
                'created_at' => Carbon::now()->addSeconds(45)
            ]
        );
    }
}
