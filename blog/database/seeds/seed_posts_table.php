<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class seed_posts_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert(
                [
                    'title' => 'Hello World!',
                    'summary' => 'This is my first post. It was created using Seeder',
                    'body' => "It is a long established fact that a reader will be distracted 
                                by the readable content of a page when looking at its layout. 
                                The point of using Lorem Ipsum is that it has a more-or-less 
                                normal distribution of letters, as opposed to using 'Content here, content here', 
                                making it look like readable English. Many desktop publishing packages and web page 
                                editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' 
                                will uncover many web sites still in their infancy. Various versions have evolved over 
                                the years, sometimes by accident, sometimes on purpose (injected humour and the like)",
                    'featured_image' => '900x300.jpg',
                    'num_views' => 0,
                    'created_at' => Carbon::now()
                ]
            );
        DB::table('posts')->insert(
            [
                'title' => 'My Second Post!',
                'summary' => 'This post is just following the recipe',
                'body' => "It is a long established fact that a reader will be distracted 
                            by the readable content of a page when looking at its layout. 
                            The point of using Lorem Ipsum is that it has a more-or-less 
                            normal distribution of letters, as opposed to using 'Content here, content here', 
                            making it look like readable English. Many desktop publishing packages and web page 
                            editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' 
                            will uncover many web sites still in their infancy. Various versions have evolved over 
                            the years, sometimes by accident, sometimes on purpose (injected humour and the like)",
                'featured_image' => '900x300.jpg',
                'num_views' => 0,
                'created_at' => Carbon::now()->addSeconds(5)
            ]
        );
        DB::table('posts')->insert(
            [
                'title' => 'My Third Post!',
                'summary' => 'This is my third post. It was created using Seeder too',
                'body' => "It is a long established fact that a reader will be distracted 
                            by the readable content of a page when looking at its layout. 
                            The point of using Lorem Ipsum is that it has a more-or-less 
                            normal distribution of letters, as opposed to using 'Content here, content here', 
                            making it look like readable English. Many desktop publishing packages and web page 
                            editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' 
                            will uncover many web sites still in their infancy. Various versions have evolved over 
                            the years, sometimes by accident, sometimes on purpose (injected humour and the like)",
                'featured_image' => '900x300.jpg',
                'num_views' => 0,
                'created_at' => Carbon::now()->addSeconds(10)
            ]
        );
        DB::table('posts')->insert(
            [
                'title' => 'My Fourth Post!',
                'summary' => 'This is my fourth post. It is easy to do insert using Seeder',
                'body' => "It is a long established fact that a reader will be distracted 
                            by the readable content of a page when looking at its layout. 
                            The point of using Lorem Ipsum is that it has a more-or-less 
                            normal distribution of letters, as opposed to using 'Content here, content here', 
                            making it look like readable English. Many desktop publishing packages and web page 
                            editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' 
                            will uncover many web sites still in their infancy. Various versions have evolved over 
                            the years, sometimes by accident, sometimes on purpose (injected humour and the like)",
                'featured_image' => '900x300.jpg',
                'num_views' => 0,
                'created_at' => Carbon::now()->addSeconds(15)
            ]
        );
        DB::table('posts')->insert(
            [
                'title' => 'My Fifth Post!',
                'summary' => 'This is my fifth post. I think I am learning using Seeder',
                'body' => "It is a long established fact that a reader will be distracted 
                            by the readable content of a page when looking at its layout. 
                            The point of using Lorem Ipsum is that it has a more-or-less 
                            normal distribution of letters, as opposed to using 'Content here, content here', 
                            making it look like readable English. Many desktop publishing packages and web page 
                            editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' 
                            will uncover many web sites still in their infancy. Various versions have evolved over 
                            the years, sometimes by accident, sometimes on purpose (injected humour and the like)",
                'featured_image' => '900x300.jpg',
                'num_views' => 0,
                'created_at' => Carbon::now()->addSeconds(20)
            ]
        );
    }
}
