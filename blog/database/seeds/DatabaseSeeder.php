<?php

use Illuminate\Database\Seeder;
use App\Post;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategorySeeder::class);
        $this->call(UsersSeeder::class);

        //$this->call(seed_posts_table::class);        
        factory(App\Post::class,100)->create();
        
        factory(App\User::class,20)->create();
    }
}
