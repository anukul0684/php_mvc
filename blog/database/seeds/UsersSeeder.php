<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert(
            [
                'name' => 'Ankit Kulshrestha',
                'email' => 'ankit@gmail.com',
                'email_verified_at' => now(),
                'password' => password_hash('password', PASSWORD_DEFAULT),
                'remember_token' => Str::random(10),
                'is_admin' => true
            ]
        );

    }
}
