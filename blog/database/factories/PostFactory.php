<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\Post;
use Illuminate\Support\Str;

$factory->define(Post::class, function (Faker $faker) {
    $title = $faker->sentence;
    $slug = Str::slug($title,'-');
    $body = '';
    for($i=0;$i<5;$i++) 
    {
        $body .= '<p>' . $faker->paragraph(6,10) . '</p>';
    }
    return [
        'title' => $title,
        'slug' => $slug,
        'summary' => $faker->paragraph(2,4),
        'body' => $body,
        'featured_image' => 'default.jpg', 
        'category_id' => rand(1,10),
        'created_at' => $faker->dateTimeThisYear,
    ];
});
