<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('bloghome');
});

// Route::get('/blog', function(){
//     return view('blog/index');
// });

Route::get('/blog', 'PostsController@index');

//Route::get('/blog/{slug}','PostsController@show');

Route::get('/blog/{post:slug}','PostsController@show');

Route::get('/contact','PageController@contact');

Route::post('/contact','PageController@contact_send');

Route::get('/category/{cat:name}', 'CategoryController@show');

Route::post('/blog/{post:slug}','PostsController@store_comment')->middleware(['auth']);

// Route::get('/admin','Admin\AdminController@index')->middleware(['auth','admin']);

// Route::get('/admin/categories','Admin\CategoriesController@index')->middleware(['auth','admin']);

Route::middleware(['auth','admin'])->group(function(){
    Route::get('/admin','Admin\AdminController@index');

    Route::get('/admin/categories','Admin\CategoriesController@index');

    /** CREATE A Category */

    Route::get('/admin/categories/create','Admin\CategoriesController@create');

    Route::post('/admin/categories','Admin\CategoriesController@store');

    /** EDIT a Category */

    Route::get('/admin/categories/edit/{cat:id}','Admin\CategoriesController@edit');

    Route::put('/admin/categories/edit/{cat:id}','Admin\CategoriesController@update');

    /** DELETE a Category */

    Route::delete('/admin/categories/delete/{cat:id}', 'Admin\CategoriesController@destroy');

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
