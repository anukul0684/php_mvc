<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $book_name }}</title>
    <link href="/css/app.css" rel="stylesheet"/>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <h1>{{$title}} was written by {{$author}}</h1>

                <p>This is the body of the Books view</p>
            </div>
            <div class="col-sm-6">
                <h1>This is subtitle</h1>
            </div>
        </div>
    </div>
    <script src="/js/app.js"></script>
</body>
</html>