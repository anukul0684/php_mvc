@extends('layouts/app')

@section('content')
    <h1>All Books</h1>
    <a href="/books/{{ $title ?? 'Harry Potter' }}">Details of {{ $title ?? 'Harry Potter' }}</a>
@stop()
