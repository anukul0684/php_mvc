@extends('layouts/app')

@section('content')
    <h1>Detail of Book - {{ $slug ?? 'Harry Potter' }}</h1>
    <h2>Author - {{ $author ?? 'J K Rowling' }}</h2>
@stop()