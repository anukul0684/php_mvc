<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BookController extends Controller
{
    //
    public function listBooks()
    {
        $data['title'] = ucfirst('All Books');        
        return view('books/index',$data);
    }

    /** 
     * 
     */
    // public function book($title)
    // {
    //     $title = $title;
    //     return view('books/show',compact('title'));
    // }


    public function book($title) 
    {
        //dump($request->get('p')); //dump and continue
        //dd($author); //dump and die
        //return "<h1>The author of $title is $author</h1>";
        $data['title'] = ucfirst($title);
        // $data['slug'] = ucfirst($slug);
        // $data['author'] = ucfirst($author);
        return view('books\show',$data);
    }
}
