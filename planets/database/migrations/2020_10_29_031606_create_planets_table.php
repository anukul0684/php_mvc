<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planets', function (Blueprint $table) {
            $table->id();
            $table->text('name'); //name of the planet
            $table->text('intro'); //small introdution of the planet
            $table->string('view_on_maps')->nullable(); //link on the google maps 
            $table->longText('description'); //description text about planet
            $table->string('featured_image')->nullable(); //name of the image stored in public folder
            $table->text('distance_from_sun')->nullable(); //distance from the sun
            $table->integer('position_from_sun')->nullable(); //postion number as per the orbit
            $table->text('radius'); //radius of the planet or star or natural satellite
            $table->text('diameter'); //diameter of the planet or star or natural satellite
            $table->double('orbital_period',15,2)->nullable(); //number of days taken to complete on orbit
            $table->string('mass',255); //mass of the planet or star or natural satellite
            $table->text('surface_temp'); //surface temperature of the planet or star or the natural satellite
            $table->integer('num_moons'); //number of moons of planet or star or natural satellite
            $table->text('first_recorded'); //the time when the details were recorded
            $table->enum('life_exists',array('yes','no'))->default('no'); //does life exists there or not
            $table->foreignId('category_id')->default(4); //category id from category table
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('planets');
    }
}
