<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class seed_planets_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //below are the six record inserted using seeder file in Planets table
        DB::table('planets')->insert(
            [
                'name' => 'Mercury',
                'intro' => "It’s thought that many civilizations recognized the planet Mercury 
                                as far back as 5,000 years. It was named about the Roman messenger 
                                god that was known for moving fast, because the planet orbited the 
                                sun more quickly than other planets.",
                'view_on_maps' => 'https://www.google.com/maps/space/mercury',
                'description' => "<p>For many years it was believed that Earth was the center of our 
                                    solar system with both the sun and 
                                    other planets orbiting Earth.The Greek philosopher, Heraclitus was the 
                                    first to indicate that the Sun was the center, with both Mercury and Venus 
                                    orbiting it instead of Earth. Planets are created when gravity pulls dust 
                                    and swirling gas together. Mercury was formed around 4.5 billion years ago 
                                    and became the small planet that is closest to our sun. It’s considered to 
                                    be a 'terrestrial planet' because Mercury has a central core, a rock mantle, 
                                    and a solid crust. After Earth, Mercury is the second densest planet in our 
                                    solar system. Mercury’s core is metallic with a radius of around 1,289 mi/2,074 km, 
                                    which is about 85% of the planet’s entire radius.</p> 
                                    <p>Scientists believe that they have found evidence that shows that part of the 
                                    metallic core is molten or liquid. The outer shell or mantle and crust is around 
                                    250 mi/400 km thick.</p> 
                                    <p>When you stare at the surface of Mercury, you can’t help but realize that it 
                                    resembles that of our moon. The surface is covered with a lot of craters that that 
                                    were caused by collisions with comets and meteoroids.",
                'featured_image' => 'mercury.jpg',
                'distance_from_sun' => '35.98 million mi',
                'position_from_sun' => 1,
                'radius' => '1,516 mi',
                'diameter' => '4,879 km',
                'orbital_period' => 87.97,
                'mass' => '3.285 × 10^23 kg',
                'surface_temp' => '-173 to 427',
                'num_moons' => 0,
                'first_recorded' => '14th century BCE by Assyrian astronomers',
                'life_exists' => 'no',
                'category_id' => 3,
                'created_at' => Carbon::now()
            ]
        );
        DB::table('planets')->insert(
            [
                'name' => 'Venus',
                'intro' => "Throughout history, Venus was one of the planets that many civilizations 
                            recognized. Named after the Roman goddess of love and beauty, it was 
                            also known by the Greeks as Aphrodite. All of the planets in our solar 
                            system are named after male gods or mythological creatures, with the 
                            exception of Venus.",
                'view_on_maps' => 'https://www.google.com/maps/space/venus',
                'description' => "<p>It is the only planet named after a female and it’s thought that 
                                    it was because it is the brightest planet. At one time, some of the astronomers in 
                                    the ancient past thought that Venus was actually two stars.</p> 
                                    <p>This was due to the fact that it appeared as both the morning and the evening star. 
                                    Because it was so bright, astronomers assumed that the planet itself must be beautiful.
                                    </p>
                                    <p>However, once space exploration began, scientists realized that the planet had a horrible 
                                    environment.
                                    </p>
                                    <p>Many missions have been sent to Venus but it’s almost impossible to get to the surface 
                                    of the planet due to its extremely hot temperatures.</p>
                                    <p>Around 4.5 billion years ago our solar system finally settled into its current configuration. 
                                    What this means is that there were quite a few planets that were relocating from one place to 
                                    another.</p>
                                    <p>Scientists have a theory that we may have had other planets that were either 
                                    thrown out into the universe or that collided and merged to create the current planets.</p>
                                    <p>Venus formed when swirling gas and dust was pulled by gravity. Venus is a 
                                    'terrestrial planet' and therefore has a central core, a rocky mantle and a solid crust.</p>
                                    <p>Because they share similar gravity, composition, density, mass, and size, Earth 
                                    and Venus have often been referred to as 'twins'. However, those are the only 
                                    similarities between the two planets.</p>
                                    <p>Venus’ heat, combined with the inner pressures have given the planet over 1,600 volcanoes. 
                                    While scientists think that most are dormant, there are some that may still be active.</p>
                                    <p>Venus has the honor of being the only single planet in our solar system, other than our 
                                    Earth, to have volcanic activity and the Venus has the most volcanoes. This makes it the 
                                    single planet with the most volcanoes in the solar system.</p>
                                    <p>Due to its slow rotation on its axis, one day on Venus takes 243 Earth-days to complete 
                                    one rotation.</p>
                                    <p>It is thought that billions of years ago, Venus might have had a similar climate as 
                                    the one on Earth. Scientists think that there may have been large amounts of water, even 
                                    big enough to create oceans on Venus.</p>
                                    <p>However, the high temperatures on Venus that produce the extreme greenhouse effect, 
                                    allowed the water to boil off. This left a planet that is too hostile and hot to sustain 
                                    life as we know it.",
                'featured_image' => 'venus.jpg',
                'distance_from_sun' => '67.24 million mi',
                'position_from_sun' => 2,
                'radius' => '3,760 mi',
                'diameter' => '7,520.8 mi',
                'orbital_period' => 224.70,
                'mass' => '4.867 × 10^24 kg',
                'surface_temp' => '465',
                'num_moons' => 0,
                'first_recorded' => '17th century BCE by Babylon astronomers',
                'life_exists' => 'no',
                'category_id' => 3,
                'created_at' => Carbon::now()
            ]
        );
        DB::table('planets')->insert(
            [
                'name' => 'Earth',
                'intro' => "It’s believed that the name 'Earth' is around 1,000 years old. If you notice, except 
                            for Earth, all of the planets in our solar system are named after Roman 
                            and Greek gods and goddesses. One idea of the source of the name is that 
                            the word 'Earth' is an old Germanic word that really just means 'the ground.'",
                'view_on_maps' => 'https://www.google.com/maps/space/earth',
                'description' => "<p>Another thought is that the name 'Earth' is derived from the Old English word 'ertha' 
                                    as well as the Anglo-Saxon word 'erda', which means soil or ground.</p> 
                                    
                                    <p>Earth is the only known planet to support life and was formed 
                                    around 4.54 billion years ago.</p>

                                    <p>Astronomers of ancient times thought that the Earth was the center of the 
                                    universe and the sun as well as all of the planets orbited around Earth.</p>

                                    <p>They also thought that the Earth was static and never changed or moved. 
                                    This idea was part of many cultures for over 2,000 years. It wasn’t until 
                                    1543 when Copernicus created his published work of the Sun-centered Solar 
                                    System model that fellow scientists realized that the sun was the center 
                                    of the solar system and Earth revolved around it.</p>

                                    <p>About 4.5 billion years ago, our solar system settled into the layout 
                                    that we see today. It’s believed that Earth formed when gravity began pulling 
                                    dust and swirling gas together, and the Earth became the third planet from the sun. 
                                    Earth is one of the terrestrial planets, which means it has a central core, 
                                    rocky mantle, and a crust that is solid.</p>

                                    <p>Our Earth orbits the sun every 23.9 hours and it takes 365.23 days to complete 
                                    an entire trip around the sun. The axis of rotation of the Earth is tilted 23.4 
                                    degrees and it’s this tilt that gives us our seasonal cycles every year.</p>

                                    <p>The Earth is made up of four main layers, beginning with the planet’s 
                                    inner core which is enveloped by the outer core, then the mantle, and finally 
                                    the crust. The inner core is around 759 mi/1,221 km in radius and is a solid 
                                    sphere of nickel and iron metals.</p>

                                    <p>The temperature of the inner core is as high as 9,800 degrees F/5,400 degrees 
                                    C. Surrounding the inner core is the outer core, which is 1,400 mi/2,300 km thick.
                                    The outer core is made up of iron and nickel fluids.</p>

                                    <p>Wedged in between the outer core and the crust is the mantle which is the 
                                    thickest of all of the layers. This is a hot, thick mixture of molten rock that 
                                    has the consistency of caramel and is around 1,800 mi/2,900 km thick.</p>

                                    <p>The Earth’s crust is the outermost layer and is an average of about 19 mi/30 km 
                                    deep on land. The ocean’s bottom has a thinner crust and it extends about 3 mi/5 km 
                                    from the floor of the sea to the top of the mantle.</p>

                                    <p>Just like Venus and Mars, Earth has mountains, volcanoes, and valleys. 
                                    The 'lithosphere' of the Earth includes both the oceanic and continental crust 
                                    as well as the upper mantle.</p>
                                    
                                    <p>It is divided into huge plates called 'tectonic plates' that are constantly 
                                    in motion. This movement causes the plates to collide to create mountains, split 
                                    or separate, or rub against each other and create earthquakes.</p>
                                    
                                    <p>The Earth has an ocean that covers almost 70% of the surface of the planet. 
                                    The average depth of the ocean is around 2.5 mi/4 km and contains 97% of the water 
                                    on the Earth.</p>
                                    
                                    <p>Almost every one of the volcanoes on Earth is hidden under these oceans. 
                                    The Mauna Kea volcano in Hawaii is taller from the base to the top than Mount Everest, 
                                    but a majority of the volcano is underwater.</p>

                                    <p>The longest mountain range on Earth is also underwater, at the bottom of the Atlantic 
                                    and Arctic oceans. This mountain range is four times longer than the Rockies, Andes, 
                                    and Himalayas all combined.</p>
                                    
                                    <p>The land masses that make up 30% of the Earth’s surface are incredibly varied. 
                                    Land has continents, islands, and other land masses as well as sources of fresh water.</p>
                                    
                                    <p>Many scientists believe that water was delivered to the Earth by comets and asteroids 
                                    as it was forming and that much of the water was inside the planet and then brought 
                                    up to the surface during volcanic activity.",
                'featured_image' => 'earth.jpg',
                'distance_from_sun' => '92.96 million mi',
                'position_from_sun' => 3,
                'radius' => '3,959 mi',
                'diameter' => '12,714 km',
                'orbital_period' => 365.24,
                'mass' => '5.972 × 10^24 kg',
                'surface_temp' => '-89 to 58',
                'num_moons' => 1,
                'first_recorded' => '1000 years ago as earth',
                'life_exists' => 'yes',
                'category_id' => 3,
                'created_at' => Carbon::now()
            ]
        );
        DB::table('planets')->insert(
            [
                'name' => 'Mars',
                'intro' => "Mars is the fourth planet from the Sun and is one of the terrestrial planets. 
                            It has a distinctive red color and was therefore associated with battles 
                            and war and named after the Roman god of war.",
                'view_on_maps' => 'https://www.google.com/maps/space/mars',
                'description' => "<p>The surface of Mars is a reddish-brown color due to the rusting process 
                                    of the surface minerals. Another name for Mars is 'The Red Planet'. Mercury 
                                    is the smallest planet in our solar system and Mars is the second smallest planet.</p> 
                                    <p>Other civilizations throughout history have also named the planet 
                                    due to its color. The Ancient Egyptians called Mars 'Her Desher,' 
                                    which translates to 'the red one'.</p> 
                                    <p>Today we often call Mars the 'Red Planet' due to the iron minerals
                                     on the surface. It’s the iron in the Martian dirt that has oxidized or 
                                     rusted that causes it to look red.</p> 
                                    <p>Around 4.5 billion years ago our solar system settled into the configuration 
                                    that we see today.</p>
                                    <p>Mars was formed when the swirling gas and dust coalesced due to gravitational 
                                    pull and created the fourth plant from the sun.</p>
                                    <p>As we explore other solar systems we see that many of their larger gas 
                                    giants are those closer to the inner orbits of the sun and our solar 
                                    system differs as we have terrestrial planets that are closer.</p>
                                    <p>Mars is a terrestrial planet with a central core, rocky mantle, 
                                    and a solid crust.</p>
                                    <p>The core at Mars’ center is very dense and is between 930 and 
                                    1,300 mi/1,500 to 2,100 km in radius.</p>
                                    <p>The core is made up of iron, nickel and sulfur. Surrounding the 
                                    core is the planet’s rocky mantle which is between 
                                    770-1,170 mi/1,240-1,880 km thick.</p>
                                    <p>Above the rocky mantle is the crust made up of iron, magnesium, 
                                    aluminum, calcium, and potassium. The crust is between 6-30 mi/10-50 km deep.</p>
                                    <p>The Mars surface is divided by the hemisphere of the planet 
                                    and is in two distinct types of features.</p>
                                    <p>Thanks to the Mars Rovers we have been able to see the smooth 
                                    look of the northern hemisphere and can see that it doesn’t have 
                                    very many craters.",
                'featured_image' => 'mars.jpg',
                'distance_from_sun' => '141.6 million mi',
                'position_from_sun' => 4,
                'radius' => '2,106 mi',
                'diameter' => '6,752 km',
                'orbital_period' => 686.97,
                'mass' => '6.39 × 10^23 kg',
                'surface_temp' => '-153 to 20',
                'num_moons' => 2,
                'first_recorded' => '2nd Millennium BCE by Egyptian astronomers',
                'life_exists' => 'no',
                'category_id' => 3,
                'created_at' => Carbon::now()
            ]
        );
        DB::table('planets')->insert(
            [
                'name' => 'Jupiter',
                'intro' => "Jupiter was named after the Roman king of the gods. It’s the fifth planet 
                            from the sun in our solar system and the largest planet of all. 
                            To give you an idea of how big Jupiter is, you could line up 11 Earths, 
                            side-by-side just to stretch from one side of Jupiter to the other.",                
                'description' => "<p>Jupiter’s mass is incredibly large as well and it would 
                                    take 317 Earths to match the mass of Jupiter.</p> 
                                    <p>Around 4.5 billion years ago our solar system settled 
                                    into the configuration that we know today. Jupiter settled in 
                                    its position as the fifth planet about 4 billion years ago.</p> 
                                    <p>Gravity played a major part in creating the planets as it pulls 
                                    dust and swirling gas together. Jupiter is one of the gas giants 
                                    and it’s believed that it took most of the mass that was left after 
                                    the sun was formed.</p> 
                                    <p>This is why Jupiter has over twice the materials of all of the 
                                    other bodies in the solar system combined. Jupiter has the same ingredients 
                                    as our own sun and if it had grown just a bit larger it would have ignited 
                                    to become a second sun in our solar system.</p>
                                    <p>Jupiter is made up of a composition of mostly hydrogen and helium that 
                                    is very similar to our sun. As a gas giant you have to go deep into the 
                                    atmosphere to find that temperature and pressure has increased so much 
                                    that it has compressed the hydrogen gas into a liquid form.</p>
                                    <p>That hydrogen liquid gives Jupiter the largest ocean in the entire 
                                    solar system. Scientists believe that nearing halfway to Jupiter’s center 
                                    the pressure is so great that electrons are squeezed away from the hydrogen 
                                    atoms.",
                'featured_image' => 'jupiter.jpg',
                'distance_from_sun' => '483.8 million mi',
                'position_from_sun' => 5,
                'radius' => '43,441 mi',
                'diameter' => '133,709 km',
                'orbital_period' => 4332.59,
                'mass' => '1.90 × 10^27 kg (318 Earths)',
                'surface_temp' => '-148',
                'num_moons' => 79,
                'first_recorded' => '7th-8th Century BCE by Babylonian astronomers',
                'life_exists' => 'no',
                'category_id' => 4,
                'created_at' => Carbon::now()
            ]
        );
        DB::table('planets')->insert(
            [
                'name' => 'Saturn',
                'intro' => "Saturn has a diameter that is the second largest in our solar system. 
                            It is the 6th planet and since Saturn and Jupiter share so much of 
                            the makeup of their atmospheres and have similar rotations, they have 
                            been listed as 'relatives'.",                
                'description' => "<p>Saturn was given the name of the Roman father of the 
                                    god Jupiter, who was also the god of agriculture.</p>
                                    <p>It took our solar system until 4.5 billion years ago to settle 
                                    into its current rotation configuration.</p>
                                    <p>Gravity pulled all of the dust and swirling gas together to form 
                                    the gas giant of Saturn and around 4 billion years ago it settle to 
                                    the location that we see today in the outer solar system.</p>
                                    <p>Saturn, like Jupiter, is mostly made up of hydrogen and helium, 
                                    the same two major elements that make up our sun.</p>
                                    <p>Saturn is a gas giant like Jupiter and is made up of mostly 
                                    hydrogen and helium. Saturn’s central core consists of dense metals 
                                    such as nickel and iron and this is surrounded by rocky materials 
                                    and other compounds that are solidified due to the intense heat and pressure.</p>
                                    <p>Like Jupiter’s core, there is an envelope around this area of 
                                    liquid metallic hydrogen inside a layer of liquid hydrogen.</p>
                                    <p>Just as with Jupiter, Saturn shares the same characteristics of 
                                    a gas giant and doesn’t have a true surface.</p>
                                    <p>Saturn is mostly swirling liquids and gases deeper down and any 
                                    spacecraft that was sent there wouldn’t have any place to land. 
                                    The pressures and temperatures on Saturn are so intense that any 
                                    craft would be crushed, melted and vaporized.</p>
                                    <p>If you combine Jupiter and Saturn together, they make up 92% of 
                                    the entire mass of the Solar System. Saturn’s interior is incredibly 
                                    hot and can reach temperatures up to 21,000 degrees F (11,700 degrees C).",
                'featured_image' => 'saturn.jpg',
                'distance_from_sun' => '890.8 million mi',
                'position_from_sun' => 6,
                'radius' => '36,184 mi',
                'diameter' => '108,728 km',
                'orbital_period' => 10759.22,
                'mass' => '5.68 × 10^26 kg (95 Earths)',
                'surface_temp' => '-178',
                'num_moons' => 62,
                'first_recorded' => '8th Century BCE by the Assyrians',
                'life_exists' => 'no',
                'category_id' => 4,
                'created_at' => Carbon::now()
            ]
        );
        DB::table('planets')->insert(
            [
                'name' => 'Uranus',
                'intro' => "The planet Uranus is one of the gas giants and in our solar system, 
                        it’s huge diameter makes it the third largest and the fourth in mass. 
                        Uranus got its name from the father of the Roman god, Saturn. ",                
                'description' => "<p>When the astronomer, William Herschel, was trying to chart the stars, 
                                    he thought he found a disk-like object that was a comet. In reality, 
                                    he discovered Uranus and his discovery is therefore considered to be 
                                    the first in what is called 'modern history'.</p> 
                                    <p>Our solar system didn’t always look the way it does now. Early on, 
                                    planets were bouncing around, moving in location and it’s thought that 
                                    there once was actually a lot more that got tossed out our crashed into each other.</p> 
                                    <p>Around 4 billion years ago Uranus settled into its current position 
                                    and it wasn’t until 4.5 billion years ago that the planets settled into 
                                    where they are now. Gravity, pulling at dust and swirling gas created the 
                                    ice giant, Uranus. It’s thought that both Neptune and Uranus were originally 
                                    closer to the sun.</p> 
                                    <p>Uranus and Neptune are the two ice giants located in the outer solar 
                                    system. Around 80% of the mass of Uranus is made up of dense, hot fluid 
                                    of 'icy' materials including water, methane, and ammonia. These are 
                                    above a rocky core and near the core are temperatures of 9,000 degrees 
                                    F/4,892 degrees C.</p>
                                    <p>The inside of Uranus is believed to contain two layers which are the 
                                    core and a mantle. Scientists think that the core is mostly made up of 
                                    rock and ice and the mantle is around 13.3 times the mass of the Earth 
                                    and made up of water, ammonia, and other elements.</p>
                                    <p>The difference between Uranus and other gas giants also relates to 
                                    the mantle as it may be 'icy' but it’s also hot and thick. Unlike other 
                                    gas giants, Uranus doesn’t emit more energy than it gets from the Sun 
                                    and scientists are interested in finding out why Uranus generates 
                                    so little heat.</p>
                                    <p>Uranus is an ice giant made up of mostly swirling fluids so it doesn’t 
                                    really have a true surface. If we tried to send a spacecraft to Uranus 
                                    there wouldn’t be anywhere to land, and besides, the extreme temperatures 
                                    and pressures would destroy the spacecraft.",
                'featured_image' => 'uranus.jpg',
                'distance_from_sun' => '1.784 billion mi',
                'position_from_sun' => 7,
                'radius' => '15,759 mi',
                'diameter' => '49,946 km',
                'orbital_period' => 30688.5,
                'mass' => ' roughly 14.5 times that of Earth',
                'surface_temp' => '-216',
                'num_moons' => 27,
                'first_recorded' => 'March 13, 1781 by William Herschel',
                'life_exists' => 'no',
                'category_id' => 4,
                'created_at' => Carbon::now()
            ]
        );
        DB::table('planets')->insert(
            [
                'name' => 'Neptune',
                'intro' => "Neptune is the eighth planet in our solar system and is 
                            known for its beautiful blue color. It was this color that was used 
                            to name it after the Roman god of the sea. ",                
                'description' => "<p>The Greeks called their god of the sea, Poseidon. Neptune also has a diameter 
                                    that makes it the 4th largest planet and it is third largest by mass.</p> 
                                    <p>Neptune wasn’t located by just using sight, but instead became 
                                    the first planet found using mathematical calculations.</p> 
                                    <p>Urbain Le Verrier had a series of predictions that were carried 
                                    forth by Johann Galle in 1946. Le Verrier suggested that the planet 
                                    be named after the Roman god of the sea.</p> 
                                    <p>Our solar system went through a lot of changes as it was formed. 
                                    Some of the larger planets are believed to have been closer to the sun 
                                    and it’s thought that other bodies may have been tossed out or crashed 
                                    into existing planets to merge to the ones that we know today.</p>
                                    <p>Around 4 billion years ago Neptune moved from the inner area of the 
                                    solar system to its current position. It wasn’t until 4.5 billion years 
                                    ago that the planets finally settled to the configuration that we have now.</p>
                                    <p>Neptune is one of our solar system’s two ice giants. Both Neptune and 
                                    Uranus exist in the outer solar system. Around 80% of Neptune’s mass consists 
                                    of dense hot fluid of 'icy' materials such as water, ammonia, and methane.</p>
                                    <p>The fluids are around a rocky, small core. Neptune is considered to be one 
                                    of the 'giant' planets and yet is also the densest. Scientists have theorized 
                                    that there might be a superhot water ocean under the cold clouds of Neptune 
                                    that doesn’t boil away due to the high pressure that locks it inside.</p>
                                    <p>The planet doesn’t really have a solid surface as most of its mass 
                                    is made of gases.</p>
                                    <p>The hydrogen, methane, and helium goes down to deep levels and merges 
                                    with water and melted liquids that are over a solid core that has around 
                                    the same mass as the Earth.</p>
                                    <p>Neptune and Uranus have similarities when it comes to the inside of 
                                    the planets. Both have two layers: a rocky core and a dense, hot liquid 
                                    mantle made of water, ammonia, and methane.</p>
                                    <p>The rocky core of Uranus is believed to be around 1.2 times the size 
                                    of Earth’s and the mantle is thought to be between 10-15 times that of Earth’s mass.</p>
                                    <p>While both Uranus and Neptune might share some characteristics and 
                                    features, Uranus gives off as much heat as it receives from the sun and 
                                    yet Neptune gives off around 2.61 times the heat that it gets.</p>
                                    <p>This is unusual as both Neptune and Uranus have the same surface temperature.",
                'featured_image' => 'neptune.jpg',
                'distance_from_sun' => '2.793 billion mi',
                'position_from_sun' => 8,
                'radius' => '15,299 mi',
                'diameter' => '48,682 km',
                'orbital_period' => 60182,
                'mass' => '17 times the mass of Earth',
                'surface_temp' => '-218 to 200',
                'num_moons' => 14,
                'first_recorded' => 'September 23, 1846 by Urbain Le Verrier & Johann Galles',
                'life_exists' => 'no',
                'category_id' => 4,
                'created_at' => Carbon::now()
            ]
        );
        DB::table('planets')->insert(
            [
                'name' => 'Pluto',
                'intro' => 'Pluto is the 2nd closest dwarf planet to the sun and lives in the Kuiper belt. 
                            Pluto was once a planet and was the 9th planet in the solar system until it was 
                            found to be a dwarf planet instead. Pluto is the largest of the dwarf planets 
                            and has 5 moons.',
                'view_on_maps' => 'https://www.google.com/maps/space/pluto/@0,-97.1853714,15421805m/data=!3m1!1e3',
                'description' => "<p>Pluto is smaller than our moon and is a mysterious world that 
                                    has valleys, plains, craters, and possibly glaciers. It has five 
                                    moons and the biggest moon is Charon, which is almost half the size of Pluto.</p> 
                                    <p>Charon and Pluto are listed as a 'double planet' because they orbit so 
                                    close to each other. Charon is listed as the largest satellite relative to 
                                    its parent planet in our solar system.</p> 
                                    <p>It is thanks to the New Horizon spacecraft that we have learned so much 
                                    about Pluto and its complex geology.</p> 
                                    <p>In 1905, Percival Lowell, an American astronomer first discovered Pluto 
                                    when he found strange deviations in the orbits of both Uranus and Neptune. 
                                    These alterations in their orbits suggested that there was another object’s 
                                    gravity that was tugging on them.</p>
                                    <p>Lowell predicted that there was a mysterious planet in 1915 but he passed 
                                    away before he could ever find it. In 1930, Clyde Tombaugh at the Lowell 
                                    Observatory officially discovered Pluto based on the information from Lowell 
                                    and other astronomers.</p>
                                    <p>Tombaugh was actually looking for the mysterious 'Planet X' and instead 
                                    discovered Pluto. It was revealed later that Planet X didn’t exist.</p>
                                    <p>Pluto was named by an 11-year old from Oxford, England named Venetia Burney. 
                                    She had suggested to her grandfather that the new discovery be named after 
                                    the mythological Roman god of the underworld.</p>
                                    <p>Her grandfather gave the suggestion to the Lowell Observatory where they 
                                    acknowledged the new name, which coincidentally has the first two letters of 
                                    Percival Lowell’s name</p>
                                    <p>In preparation for the New Horizons space mission, scientists used the 
                                    Hubble Space Telescope to photograph Pluto in 2005. In the process, they 
                                    discovered two other small moons orbiting around Pluto, now named Nix and Hydra.</p>
                                    <p>Pluto is part of the Kuiper Belt which are objects beyond Neptune’s orbit 
                                    and are believed to have been formed 4.5 billion years ago as our solar system formed.</p>
                                    <p>The objects in this disc-like zone are rocky and icy and are referred to 
                                    as Kuiper Belt objects, plutoids, or transneptunian objects.</p>
                                    <p>Pluto has five moons: Charon, Styx, Nix, Kerberos, and Hydra. Charon is 
                                    the closest to Pluto and the largest of all of the moons, and Hydra is 
                                    the farthest away. The discovery of Charon in 1978 also showed astronomers that
                                     this moon was almost half the size of Pluto.",
                'featured_image' => 'pluto.jpg',                
                'position_from_sun' => 9,
                'radius' => '1,188.3 km',
                'diameter' => '2,376.6 km',                
                'mass' => 'one-sixth the mass of the Moon',
                'surface_temp' => '-229',
                'num_moons' => 5,
                'first_recorded' => 'February 18, 1930 by Clyde W. Tombaugh',
                'life_exists' => 'no',
                'category_id' => 2,
                'created_at' => Carbon::now()
            ]
        );

        DB::table('planets')->insert(
            [
                'name' => 'Sun',
                'intro' => 'The Sun is the star at the center of the Solar System. 
                                Our planetary system is named the "solar" system because our Sun is named 
                                Sol, after the Latin word for Sun, "solis," and anything related to the 
                                Sun we call "solar." Our planetary system is located in an outer spiral 
                                arm of the Milky Way galaxy.',                
                'description' => "<p>The Sun is a star. There are lots of stars, but the Sun is the 
                                    closest one to Earth. It is the center of our solar system.</p>                            
                                    <p>The Sun is a hot ball of glowing gases. It keeps our planet warm 
                                        enough for living things to thrive. It gives us light so we can see.</p>
                                    <p>Eight planets move around the Sun. We call that an orbit. 
                                        The planets are: Mercury, Venus, Earth, Mars, Jupiter, Saturn, 
                                        Uranus and Neptune.</p>                                      
                                    <p>The connection and interactions between the Sun and Earth drive the 
                                        seasons, ocean currents, weather, climate, radiation belts and aurorae.</p>                                                        
                                    <p>Lots of smaller worlds orbit the Sun. Pluto is a dwarf planet beyond 
                                        Neptune. There are many asteroids and comets that go around the Sun, too.",
                'featured_image' => 'sun.jpg',                
                'position_from_sun' => 0,
                'radius' => '696,342 km',
                'diameter' => '1.39 million km',                
                'mass' => '1.9885 × 10^30 kg',
                'surface_temp' => '5,505',
                'num_moons' => 0,
                'first_recorded' => '(206 BC–AD 220) by Chinese astronomers',
                'life_exists' => 'no',
                'category_id' => 6,
                'created_at' => Carbon::now()
            ]
        );
    }
}
