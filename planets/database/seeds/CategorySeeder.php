<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //below are the six record inserted using seeder file in Category table
        DB::table('categories')->insert(
            [
                'name' => 'Natural Satellite',
                'created_at' => Carbon::now()
            ]
        );
        DB::table('categories')->insert(
            [
                'name' => 'Dwarf Planet',
                'created_at' => Carbon::now()
            ]
        );
        DB::table('categories')->insert(
            [
                'name' => 'Terrestrial Planet',
                'created_at' => Carbon::now()
            ]
        );
        DB::table('categories')->insert(
            [
                'name' => 'Giant Planet',
                'created_at' => Carbon::now()
            ]
        );
        DB::table('categories')->insert(
            [
                'name' => 'Small Solar System Bodies',
                'created_at' => Carbon::now()
            ]
        );
        DB::table('categories')->insert(
            [
                'name' => 'Star',
                'created_at' => Carbon::now()
            ]
        );
    }
}
