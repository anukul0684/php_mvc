<?php

use Illuminate\Support\Facades\Route;
use App\Planet;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/** Route to go to Home page using GET method */
Route::get('/', function(){
    $title = "Planets - Home";
    $dwarf_planet = Planet::where('id',9)->with('category')->first();
    $star_at_center = Planet::where('id',10)->with('category')->first();
    //dd($dwarf_planet);
    return view('planets_home',compact('title','dwarf_planet','star_at_center'));
});

/** Route to go to About page using GET method */
Route::get('/about',function() {
    $title = "Planets - About Us";
    return view('about',compact('title'));
});

/** Route to go to Contact page using GET method */
Route::get('/contact',function() {
    $title = "Planets - Contact Us";
    return view('contact',compact('title'));
});

/** Route to go to Planets list view page using GET method */
Route::get('/planets','PlanetsController@index');

/** Route to go to Planets detail view page using GET method */
Route::get('/planets/{planet:name}','PlanetsController@show');

Route::middleware(['auth','admin'])->group(function(){
    /** GO to ADMIN DASHBOARD */

    Route::get('/admin','Admin\AdminController@index');

    /** PLANET LIST VIEW */

    Route::get('/admin/planets','Admin\PlanetController@index');

    /** Add a Planet */

    Route::get('/admin/planets/create','Admin\PlanetController@create');

    Route::post('/admin/planets','Admin\PlanetController@store');

    /** EDIT a Planet */

    Route::get('/admin/planets/edit/{planet:id}','Admin\PlanetController@edit');

    Route::put('/admin/planets/edit/{planet:id}','Admin\PlanetController@update');

    /** DELETE a PLANET */

    Route::delete('/admin/planets/delete/{planet:id}', 'Admin\PlanetController@destroy');

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
