<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Planet;
use App\Category;
use Illuminate\Support\Str;
class PlanetController extends Controller
{

    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $planets = Planet::orderBy('id')
                            ->with('category')
                            ->Paginate(6);        
        $title = 'All Planets';
        return view('admin/planets/index',compact('title','planets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $title="Add Planet";
        $cats = Category::all();
        return view('admin/planets/create',compact('title','cats'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = $request->validate(
            [
                'name' => 'bail|required|string',
                'intro' => 'bail|required|string',
                'view_on_maps' => 'bail|nullable|string',
                'description' => 'bail|required|string',
                'distance_from_sun' => 'bail|nullable|string',
                'position_from_sun' => 'bail|nullable|integer',
                'radius' => 'bail|required|string',
                'diameter' => 'bail|required|string',
                //'orbital_period' => 'bail|nullable|regex:/^([0-9]\d{1,5})(?:\.\d\d)?$/',
                'orbital_period' => 'bail|nullable|integer',
                'mass' => 'bail|required|string',
                'surface_temp' => 'bail|required|string',
                'num_moons' => 'bail|required|integer',
                'first_recorded' => 'bail|required|string',
                //'life_exists' => 'bail|required|in:yes,no',
                //'category_id' => 'bail|required|integer'                
            ]
        );
                
        //dd($valid);
        $planet = Planet::create($valid);

        if($planet) {
            session()->flash('success','Planet was added successfully');            
        } else {
            session()->flash('error','Issue in adding Planet. Try Again.');
        }
        $title = 'Add Planet';
        return redirect('/admin/planets');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Planet $planet)
    {
        //        
        $title="Edit Planet";
        $cats = Category::all();
        return view('admin/planets/edit', compact('title','planet','cats'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Planet $planet)
    {
        //
        $valid = $request->validate([
            'name' => 'bail|required|string',
            'intro' => 'bail|required|string',
            'view_on_maps' => 'bail|nullable|string',
            'description' => 'bail|required|string',
            'distance_from_sun' => 'bail|nullable|string',
            'position_from_sun' => 'bail|nullable|integer',
            'radius' => 'bail|required|string',
            'diameter' => 'bail|required|string',
            //'orbital_period' => 'bail|nullable|regex:/^([0-9]\d{1,5})(?:\.\d\d)?$/',
            'orbital_period' => 'bail|nullable|integer',
            'mass' => 'bail|required|string',
            'surface_temp' => 'bail|required|string',
            'num_moons' => 'bail|required|integer',
            'first_recorded' => 'bail|required|string',
            //'life_exists' => 'bail|required|in:yes,no',
            //'category_id' => 'bail|required|integer' 
            'featured_image' => 'nullable|image'         
        ]);

        $orig_name = $request->file('featured_image')->getClientOriginalName();
        $new_name = Str::slug(now(),'-') . '_' . $orig_name;
        
        $request->file('featured_image')->storeAs('public/images',$new_name);  

        $valid['featured_image'] = $new_name;
        //dd($planet);
        //dd($valid['featured_image']);
        if($planet->update($valid)) {
            session()->flash('success','The planet was successfully updated');
        } else {
            session()->flash('error','Some issue occurred in updating planet. Please try again');
        }
        
        
        return redirect('/admin/planets');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,Planet $planet)
    {
        //
        $valid = $request->validate([
            'id' => 'bail|required|integer'
        ]);
                    
        $planet->delete();

        session()->flash('success', 'The planet was deleted');

        return redirect()->back();
    }
}
