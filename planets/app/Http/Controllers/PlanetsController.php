<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Planet;
use App\Category;

class PlanetsController extends Controller
{
    //
    /**
     * index() function for list view of Planets
     * @return planets/index.blade.php with details of 
     *          all planets and title of the page
     */
    public function index()
    {
        //$planets = Planet::orderBy('id')->Paginate(3);
        $planets = Planet::orderBy('id')
                            ->with('category')
                            ->Paginate(3);
        //dd($planets);
        $title = 'Our Planets';
        return view('planets/index',compact('title','planets'));
    }

    /**
     * show(Planet $planet) function for getting details of a particular planet
     * @param $planet object that contains the record of the planet binded with its name
     * @return planets/show.blade.php with details of category of the planet, 
     *          planet record and title of the page
     */
    public function show(Planet $planet)
    {
        $cat = Category::find($planet->category_id);
        $title = "Planets" . " - " . $planet->name;
        return view('planets/show',compact('title','planet','cat'));
    }
}
