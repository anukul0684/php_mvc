<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;

class Planet extends Model
{
    protected $fillable = [
        'name',
        'intro',
        'view_on_maps',
        'description',
        'distance_from_sun',
        'position_from_sun',
        'radius',
        'diameter',
        'orbital_period',
        'mass',
        'surface_temp',
        'num_moons',
        'first_recorded',
        'life_exists',
        'featured_image',
        'category_id'
    ];
    //
    /**
     * category() function that sets relation between Planets and Category
     * @return details of category that is associated with particular planet
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

}
