<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Planet;

class Category extends Model
{
    //
    /**
     * planets() function that sets relation between Category and Planet
     * @return Collection of planets that are associated with particular category id
     */
    public function planets()
    {
        return $this->hasMany(Planet::Class);
    }
}
