@extends('layouts/app')

@section('content')
    <div class="container">       
        <p class="card-text mb-0 text-center">
            <a href="/admin/planets/create" class="btn btn-success mb-4 btn-lg">Add Planet</a>
        </p>        
        @include('partials/flash')
        <!-- for loop for listing all planets in below format -->
        @foreach($planets as $planet)
        
            <div class="card col-lg-12 mb-3 text-dark">
                <div class="row no-gutters py-2">
                    <div class="col-md-2">
                        <a href="/admin/planets/edit/{{ $planet->id }}" title="Click here to view details">
                            <img src="/storage/images/{{$planet->featured_image}}" 
                                class="img-thumbnail" alt="{{ $planet->name }}"/>
                        </a>
                    </div>
                    <div class="col-md-8">
                        <div class="card-body lead">
                            <h5 class="card-title"><strong>{{ $planet->name }}</strong></h5>
                            <p class="card-text">
                                {{ $planet->intro }}                                    
                            </p>
                            <p class="card-text mb-0 mt-0">
                                <strong>First Recorded</strong>&#58; {{ $planet->first_recorded }}
                            </p>
                            <p class="card-text mb-1 mt-0">
                                <strong>Category</strong>&#58; {{ $planet->category->name }}
                            </p>                                
                            <p class="card-text mb-0 mt-0">
                                <small class="text-muted">Last updated {{$planet->updated_at ?? 'Not Updated Yet'}}</small>
                            </p>                            
                        </div>
                    </div>
                    <div class="col-md-2 text-center">
                        <p class="card-text mb-0">                                
                            <a class="btn btn-primary btn-lg mb-4 mt-4" href="/admin/planets/edit/{{ $planet->id }}">Edit</a>                                
                            <form class="form" action="/admin/planets/delete/{{$planet->id}}" method="post">
                                @csrf 
                                @method('DELETE')
                                <input type="hidden" name="id" value="{{$planet->id}}" />
                                <button class="btn btn-danger btn-lg">Delete</button>
                            </form>                                
                        </p>
                    </div>
                </div>
            </div>
        
        @endforeach
        <!-- for loop ends here -->
        
        <!-- Pagination on list view -->
        <div class="pagination justify-content-center mb-4">            
            {{ $planets->links() }}
        </div>
        
    </div>
@stop()