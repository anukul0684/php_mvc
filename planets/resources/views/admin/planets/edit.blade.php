@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header mb-4">{{ __('Admin Dashboard') }}</div>
                    @include('partials/flash')
                    <div class="card-body">
                        <h2>{{ $title }}</h2>
                        <p><a href="/admin/planets" class="btn btn-primary">Back</a></p>
                        <form class="form" action="/admin/planets/edit/{{$planet->id}}" 
                                enctype="multipart/form-data" method="post" novalidate>
                            @csrf
                            @method('PUT')
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    @if($planet->featured_image)
                                        <img src="/storage/images/{{$planet->featured_image}}" 
                                                alt="{{$planet->featured_image}}" />
                                    @endif
                                    <label for="featured_image">Icon</label>
                                    <input type="file" name="featured_image" />
                                    @error('featured_image')
                                        <div class="alert alert-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>                                
                                <div class="form-group col-md-6">
                                    <input type="hidden" name="id" value="{{old('id',$planet->id)}}" />
                                    <label for="name">Name</label>
                                    <input class="form-control" type="text" 
                                            name="name" value="{{old('name', $planet->name)}}"/>
                                    @error('name')
                                        <div class="alert alert-danger">
                                        {{$message}}
                                        </div>
                                    @enderror
                                </div>   
                            </div>    
                                                 
                            <div class="form-group">
                                <label for="intro">Intro</label>
                                <input class="form-control" type="text" 
                                            name="intro" value="{{old('intro', $planet->intro)}}"/>
                                @error('intro')
                                    <div class="alert alert-danger">
                                    {{$message}}
                                    </div>
                                @enderror
                            </div>                           
                            <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea id="description" name="description" 
                                    rows="4" 
                                    class="form-control">{{ strip_tags(str_replace('  ', ' ', old('description', $planet->description))) }}</textarea>                                    
                                </div>                                                           
                                @error('description')
                                    <div class="alert alert-danger">
                                    {{$message}}
                                    </div>
                                @enderror
                            <div class="form-group">                                                           
                                <label for="view_on_maps">Links on Maps</label>
                                    <input class="form-control" type="text" 
                                            name="view_on_maps" 
                                            value="{{old('view_on_maps', $planet->view_on_maps)}}"/>
                                @error('view_on_maps')
                                    <div class="alert alert-danger">
                                    {{$message}}
                                    </div>
                                @enderror                                                                
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">                                
                                    <label for="distance_from_sun">Distance from Sun</label>
                                    <input class="form-control" type="text" 
                                            name="distance_from_sun" 
                                            value="{{old('distance_from_sun', $planet->distance_from_sun)}}"/>
                                    @error('distance_from_sun')
                                        <div class="alert alert-danger">
                                        {{$message}}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">                                
                                    <label for="position_from_sun">Position from Sun</label>
                                    <input class="form-control" type="text" 
                                            name="position_from_sun" 
                                            value="{{old('position_from_sun', $planet->position_from_sun)}}"/>
                                    @error('position_from_sun')
                                        <div class="alert alert-danger">
                                        {{$message}}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">                                
                                    <label for="radius">Radius</label>
                                    <input class="form-control" type="text" 
                                            name="radius" 
                                            value="{{old('radius', $planet->radius)}}"/>
                                    @error('radius')
                                        <div class="alert alert-danger">
                                        {{$message}}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">                                
                                    <label for="diameter">Diameter</label>
                                    <input class="form-control" type="text" 
                                            name="diameter" 
                                            value="{{old('diameter', $planet->diameter)}}"/>
                                    @error('diameter')
                                        <div class="alert alert-danger">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">                                
                                    <label for="orbital_period">Orbital Period (in days)</label>
                                    <input class="form-control" type="text" 
                                            name="orbital_period" 
                                            value="{{old('orbital_period', $planet->orbital_period)}}"/>
                                    @error('orbital_period')
                                        <div class="alert alert-danger">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">                                
                                    <label for="mass">Mass</label>
                                    <input class="form-control" type="text" 
                                            name="mass" 
                                            value="{{old('mass', $planet->mass)}}"/>
                                    @error('mass')
                                        <div class="alert alert-danger">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">                                
                                    <label for="surface_temp">Surface Temp</label>
                                    <input class="form-control" type="text" 
                                            name="surface_temp" 
                                            value="{{old('surface_temp', $planet->surface_temp)}}"/>
                                    @error('surface_temp')
                                        <div class="alert alert-danger">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">                                
                                    <label for="num_moons">Number of Moons</label>
                                    <input class="form-control" type="text" 
                                            name="num_moons" 
                                            value="{{old('num_moons', $planet->num_moons)}}"/>
                                    @error('num_moons')
                                        <div class="alert alert-danger">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">                                
                                    <label for="first_recorded">First Recorded</label>
                                    <input class="form-control" type="text" 
                                            name="first_recorded" 
                                            value="{{old('first_recorded', $planet->first_recorded)}}"/>
                                    @error('first_recorded')
                                        <div class="alert alert-danger">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">                                
                                    <label for="life_exists">Life Exists</label>
                                    <input class="form-control" type="text" 
                                            name="life_exists" 
                                            value="{{old('life_exists', $planet->life_exists)}}"/>
                                    @error('life_exists')
                                        <div class="alert alert-danger">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>  
                            </div>                            
                            <div class="group">     
                                <div class="col-sm-2">Category    
                                </div>                       
                                <div class="col-sm-10">
                                    @foreach($cats as $cat)
                                        @if($cat->id == $planet->category_id)
                                            <div class="form-check-inline">                                            
                                                <input class="form-check-input" type="radio" 
                                                    name="category" id="{{$cat->id}}" 
                                                    value="{{$cat->id}}" checked disabled>
                                                <label class="form-check-label" for="category">
                                                    {{$cat->name}}
                                                </label>                                            
                                            </div>
                                            @continue
                                        @endif
                                        <div class="form-check-inline">                                            
                                            <input class="form-check-input" type="radio" 
                                                name="category" id="{{$cat->id}}" 
                                                value="{{$cat->id}}" disabled>
                                            <label class="form-check-label" for="category">
                                                {{$cat->name}}
                                            </label>                                            
                                        </div>
                                    @endforeach  
                                    @error('category')
                                        <div class="alert alert-danger">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>
                                </div>
                            </fieldset>                                       
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
