@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header mb-4">{{ __('Admin Dashboard') }}</div>
                    @include('partials/flash')
                    <div class="card-body">
                        <h2>{{ $title }}</h2>
                        <p><a href="/admin/planets" class="btn btn-primary">Back</a></p>
                        <form class="form" action="/admin/planets" 
                                enctype="multipart/form-data" method="post" novalidate>
                            @csrf
                            
                            <div class="form-row">
                                
                                <div class="form-group col-md-12">
                                    
                                    <label for="name">Name</label>
                                    <input class="form-control" type="text" 
                                            name="name" placeholder="Enter Name for Planet"/>
                                    @error('name')
                                        <div class="alert alert-danger">
                                        {{$message}}
                                        </div>
                                    @enderror
                                </div>   
                            </div>    
                                                 
                            <div class="form-group">
                                <label for="intro">Intro</label>
                                <input class="form-control" type="text" 
                                            name="intro" placeholder="Enter Intro for Planet"/>
                                @error('intro')
                                    <div class="alert alert-danger">
                                    {{$message}}
                                    </div>
                                @enderror
                            </div>                           
                            <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea id="description" name="description" 
                                    rows="4" onfocus="this.value=''; return false;"
                                    class="form-control">Enter Intro for Planet</textarea>                                    
                                </div>                                                           
                                @error('description')
                                    <div class="alert alert-danger">
                                    {{$message}}
                                    </div>
                                @enderror
                            <div class="form-group">                                                           
                                <label for="view_on_maps">Links on Maps</label>
                                    <input class="form-control" type="text" 
                                            name="view_on_maps" 
                                            placeholder="Enter links for maps for Planet"/>
                                @error('view_on_maps')
                                    <div class="alert alert-danger">
                                    {{$message}}
                                    </div>
                                @enderror                                                                
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">                                
                                    <label for="distance_from_sun">Distance from Sun</label>
                                    <input class="form-control" type="text" 
                                            name="distance_from_sun" 
                                            placeholder="Enter Distance from Sun of Planet"/>
                                    @error('distance_from_sun')
                                        <div class="alert alert-danger">
                                        {{$message}}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">                                
                                    <label for="position_from_sun">Position from Sun</label>
                                    <input class="form-control" type="text" 
                                            name="position_from_sun" 
                                            placeholder="Enter Position from Sun of Planet"/>
                                    @error('position_from_sun')
                                        <div class="alert alert-danger">
                                        {{$message}}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">                                
                                    <label for="radius">Radius</label>
                                    <input class="form-control" type="text" 
                                            name="radius" 
                                            placeholder="Enter Radius of Planet"/>
                                    @error('radius')
                                        <div class="alert alert-danger">
                                        {{$message}}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">                                
                                    <label for="diameter">Diameter</label>
                                    <input class="form-control" type="text" 
                                            name="diameter" 
                                            placeholder="Enter Diameter of Planet"/>
                                    @error('diameter')
                                        <div class="alert alert-danger">
                                        {{$message}}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">                                
                                    <label for="orbital_period">Orbital Period (in days)</label>
                                    <input class="form-control" type="text" 
                                            name="orbital_period" 
                                            placeholder="Enter Orbital Period of Planet around Sun"/>
                                    @error('orbital_period')
                                        <div class="alert alert-danger">
                                        {{$message}}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">                                
                                    <label for="mass">Mass</label>
                                    <input class="form-control" type="text" 
                                            name="mass" 
                                            placeholder="Enter Mass of Planet"/>
                                    @error('mass')
                                        <div class="alert alert-danger">
                                        {{$message}}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">                                
                                    <label for="surface_temp">Surface Temp</label>
                                    <input class="form-control" type="text" 
                                            name="surface_temp" 
                                            placeholder="Enter Surface Temperature of Planet"/>
                                    @error('surface_temp')
                                        <div class="alert alert-danger">
                                        {{$message}}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">                                
                                    <label for="num_moons">Number of Moons</label>
                                    <input class="form-control" type="text" 
                                            name="num_moons" 
                                            placeholder="Enter Number of Moons of Planet"/>
                                    @error('num_moons')
                                        <div class="alert alert-danger">
                                        {{$message}}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">                                
                                    <label for="first_recorded">First Recorded</label>
                                    <input class="form-control" type="text" 
                                            name="first_recorded" 
                                            placeholder="Enter Very First details known for Planet"/>
                                    @error('first_recorded')
                                        <div class="alert alert-danger">
                                        {{$message}}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">                                
                                    <label for="life_exists">Life Exists</label>
                                    <input class="form-control" type="text" 
                                            name="life_exists" 
                                            placeholder="Enter if life exists in either yes or no on Planet"/>
                                    @error('life_exists')
                                        <div class="alert alert-danger">
                                        {{$message}}
                                        </div>
                                    @enderror
                                </div>  
                            </div>                            
                            <div class="group">     
                                <div class="col-sm-2">Category    
                                </div>                       
                                <div class="col-sm-10">
                                    @foreach($cats as $cat)                                        
                                    <div class="form-check-inline">                                            
                                            <input class="form-check-input" type="radio" 
                                                name="category" id="{{$cat->id}}" 
                                                value="{{$cat->id}}">
                                            <label class="form-check-label" for="category">
                                                {{$cat->name}}
                                            </label>                                            
                                        </div>
                                    @endforeach  
                                    @error('category')
                                        <div class="alert alert-danger">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>
                                </div>
                            </fieldset>                                       
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
