@extends('layouts/planets')

@section('content')
    <div class="container">        
        <!-- for loop for listing all planets in below format -->
        @foreach($planets as $planet)
        
            <div class="card col-lg-12 mb-3 text-dark">
                <div class="row no-gutters py-2">
                    <div class="col-md-4">
                        <a href="/planets/{{ $planet->name }}" title="Click here to view details">
                            <img src="/images/{{$planet->featured_image}}" class="card-img" alt="{{ $planet->name }}">
                        </a>
                    </div>
                    <div class="col-md-8">
                        <div class="card-body lead">
                            <h5 class="card-title"><strong>{{ $planet->name }}</strong></h5>
                            <p class="card-text">
                                {{ $planet->intro }}                                    
                            </p>
                            <p class="card-text mb-0">
                                <strong>First Recorded</strong>&#58; {{ $planet->first_recorded }}
                            </p>
                            <p class="card-text mb-1">
                                <strong>Category</strong>&#58; {{ $planet->category->name }}
                            </p>                                
                            <p class="card-text"><small class="text-muted">Last updated {{$planet->created_at}}</small></p>
                            <p class="card-text mb-0 text-right">
                                <a class="btn btn-danger" href="/planets/{{ $planet->name }}">Read More</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        
        @endforeach
        <!-- for loop ends here -->
        
        <!-- Pagination on list view -->
        <div class="pagination justify-content-center mb-4">            
            {{ $planets->links() }}
        </div>
        
    </div>
@stop()