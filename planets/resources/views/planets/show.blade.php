@extends('layouts/planets')

@section('content')
    <div class="container">
        <div class="d-flex flex-row text-dark">
            <div id="leftdiv" class="col-sm-6">
                <div class="card mt-4">
                    <img class="card-img-top img-fluid" 
                            src="/images/{{substr($planet->featured_image,0,strpos($planet->featured_image,'.')) . 'B.jpg'}}" 
                            alt="{{$planet->name}}">            
                
                    <div class="card-body lead bg-light rounded-bottom">                    
                        <p class="card-text mb-0 mt-4">
                            <strong>Position from Sun</strong>&#58; 
                            @if($planet->position_from_sun != '0')
                                {{ $planet->position_from_sun }}
                            @else
                                {{ 'Not Applicable' }}
                            @endif
                        </p>
                        <p class="card-text mb-0">
                            <strong>Distance from Sun</strong>&#58; 
                            {{$planet->distance_from_sun ?? 'Not Available/Applicable'}}
                        </p>
                        <p class="card-text mb-0">
                            <strong>Number of Moons</strong>&#58; {{$planet->num_moons}}
                        </p>
                        <p class="card-text mb-0">
                            <strong>Life Exists Here</strong>&#58; {{$planet->life_exists}}
                        </p>
                        <p class="card-text mb-0">
                            <strong>Category</strong>&#58; {{$cat->name}}
                        </p>  
                        <p class="card-text mb-0">
                            <strong>Orbital Period</strong>&#58; 
                            @if($planet->orbital_period)
                                {{ $planet->orbital_period . ' days' }}
                            @else
                                {{'Not Available/Applicable'}} 
                            @endif
                        </p> 
                        <p class="card-text mb-2">
                            <strong>View on Maps</strong>&#58; 
                            <a class="text-dark" href="{{$planet->view_on_maps ?? '#'}}" 
                                target="_blank">{{$planet->view_on_maps ?? 'Not Available'}}</a>
                        </p>                   
                        
                    </div>
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col-lg-6 -->

            <div id="rightdiv" class="col-sm-6">
                <div class="card card-outline-secondary lead mt-4">
                    <div class="card-header bg-secondary text-light">
                        <h5 class="card-title"><strong>{{$planet->name}}</strong></h5>                                    
                    </div>
                    <div class="card-body bg-light mb-0 mt-4">       
                        <div class="overflow-auto" style="max-height: 535px;">         
                            <p class="card-text mb-0" >
                                {!! $planet->description !!}
                            </p>                        
                        </div>
                        <p class="card-text mb-0 mt-2">
                            <strong>Diameter</strong>&#58; {{$planet->diameter}}
                        </p>
                        <p class="card-text mb-0">
                            <strong>Mass</strong>&#58; {{$planet->mass}}
                        </p>
                        <p class="card-text mb-0">
                            <strong>Surface Temperature</strong>&#58; {{$planet->surface_temp}} &#8451;
                        </p>  
                        <p class="card-text mb-2">
                            <strong>First Recorded</strong>&#58; {{$planet->first_recorded}}
                        </p>
                        <p class="card-text mb-0 text-right">                                              
                            <a href="/planets" class="btn btn-success">Back to Planets</a>
                        </p>
                    </div>
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col-lg-6 -->
        </div>
    </div>
    
@stop()