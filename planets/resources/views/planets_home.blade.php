@extends('layouts/planets')

@section('content')
    <!-- Jumbotron Header -->
    <header class="jumbotron my-3">
        <h1 class="display-3 text-dark">Our Solar System!</h1>
        <p class="lead text-dark">
            The Solar System is the gravitationally bound system of the Sun and the objects that orbit it, 
            either directly or indirectly. Of the objects that orbit the Sun directly, the largest are the 
            eight planets, with the remainder being smaller objects, the dwarf planets and small Solar 
            System bodies.
        </p>
        <p class="lead text-dark">
            Let's <a href="/planets" class="btn btn-danger btn-lg">Learn</a> 
            more about them!
        </p>
    </header>
    <!-- section to show the home page view -->
    <section id="services" class="bg-light">
        <div class="container">        
            <div class="row text-center">
                <div class="col-lg-4 col-md-6 mb-4 mt-4">
                    <div class="card h-100">    
                        <a href="/planets/{{ $star_at_center->name }}" title="Click here to view details">                    
                            <img class="card-img-top" 
                                src="/images/{{substr($star_at_center->featured_image,0,strpos($star_at_center->featured_image,'.')) . 'B.jpg'}}" 
                                alt="{{$star_at_center->name}}">
                        </a>
                        <div class="card-body text-dark">
                        <h4 class="card-title ">Star and Center</h4>
                        <p class="card-text">
                            {{$star_at_center->intro}}
                        </p>
                        </div>
                        <div class="card-footer">
                        <a href="/planets/{{$star_at_center->name}}" class="btn btn-danger">Find Out More!</a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 mb-4 mt-4">
                    <div class="card h-100">
                        <a href="/planets" title="Click here to view details">
                            <img class="card-img-top" src="/images/coverB.jpg" alt="Planets in our Solar System">
                        </a>
                        <div class="card-body text-dark">
                        <h4 class="card-title">Our Planets</h4>
                        <p class="card-text">
                            Here are the planets listed in order of their distance 
                            from the Sun: Mercury, Venus, Earth, Mars, Jupiter, Saturn, Uranus, and Neptune. 
                            An easy mnemonic for remembering the order is 'My Very Educated Mother 
                            Just Served Us Noodles.'
                        </p>
                        </div>
                        <div class="card-footer">
                        <a href="/planets" class="btn btn-danger">Find Out More!</a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 mb-4 mt-4">
                    <div class="card h-100">
                        <a href="/planets/{{ $dwarf_planet->name }}" title="Click here to view details">
                            <img class="card-img-top" 
                                src="/images/{{substr($dwarf_planet->featured_image,0,strpos($dwarf_planet->featured_image,'.')) . 'B.jpg'}}" 
                                alt="{{$dwarf_planet->name}}">
                        </a>
                        <div class="card-body text-dark">
                        <h4 class="card-title">The Dwarf Planet</h4>
                        <p class="card-text">
                            {{$dwarf_planet->intro}}
                        </p>
                        </div>
                        <div class="card-footer">
                        <a href="/planets/{{$dwarf_planet->name}}" class="btn btn-danger">Find Out More!</a>
                        </div>
                    </div>
                </div>                
            </div>            
        </div>
    </section>
@stop()