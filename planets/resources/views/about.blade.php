@extends('layouts/planets')

@section('content')
    <header class="jumbotron my-3">
        <h1 class="display-3 text-dark">About Us</h1>
        <p class="lead text-dark">
            A mother, writer, web developer, dancer, singer, and a soulful cook. I have created this 
            website as an inspiration for my son, Anish. The actual story behind collecting all the
            data in this site is the collection of thoughts my son speaks in front of all and shows
            his fondness about our solar system. In a way he inspired me to collect the inputs about 
            sun, eight planets, and the dwarf planet, pluto.   
        </p>
        <p class="lead text-dark">
            All the data displayed on this website is the collection of information gathered from various 
            studies available on internet. I am so grateful to my institute, PACE, UofW, because of 
            them I could get this opportunity to learn and create a piece of work for a topic so close to
            my heart.
        </p>
        <p class="lead text-dark">
            "Us" in "About Us" includes all the support and training provided by our instructors.
        </p>        
    </div>
    </header>
@stop()