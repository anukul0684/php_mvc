@extends('layouts/planets')

@section('content')
    <!--Section heading-->
    <header class="jumbotron my-3 text-dark">
    <h1 class="display-3 text-dark text-center">Contact Us</h1>
    <!--Section description-->
    <p class="text-center w-responsive mx-auto mb-5 lead">Do you have any questions? 
        Please do not hesitate to contact us directly. Our team will come back to you within
        a matter of hours to help you.</p>

    <div id="contact_page" class="row">

        <!--Grid column-->
        <div id="contact_form" class="col-md-7 mb-5 pt-5 ml-0 mr-0 border border-dark rounded">
            <form id="contact-form" name="contact-form" action="#" method="POST">

                <!--Grid row-->
                <div class="row">

                    <!--Grid column-->
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <input type="text" id="name" name="name" class="form-control">
                            <label for="name" class="">Your name</label>
                        </div>
                    </div>
                    <!--Grid column-->

                    <!--Grid column-->
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <input type="text" id="email" name="email" class="form-control">
                            <label for="email" class="">Your email</label>
                        </div>
                    </div>
                    <!--Grid column-->

                </div>
                <!--Grid row-->

                <!--Grid row-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="md-form mb-0">
                            <input type="text" id="subject" name="subject" class="form-control">
                            <label for="subject" class="">Subject</label>
                        </div>
                    </div>
                </div>
                <!--Grid row-->

                <!--Grid row-->
                <div class="row mb-4">

                    <!--Grid column-->
                    <div class="col-md-12">

                        <div class="md-form">
                            <textarea id="message" name="message" rows="2" class="form-control md-textarea"></textarea>
                            <label for="message">Your message</label>
                        </div>

                    </div>
                </div>
                <!--Grid row-->

            </form>

            <div class="text-center text-md-left">
                <a class="btn btn-primary" onclick="document.getElementById('contact-form').submit();">Send</a>
            </div>
            <div class="status mb-4"></div>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div id="contact_details" class="col-md-5 text-center lead">
            <ul class="list-unstyled mb-0 mt-4 ml-0 mr-0">
                <li><i class="fas fa-map-marker-alt fa-2x mb-4 mt-4"></i>
                    <a href="https://goo.gl/maps/moveSrtHwWNJhfW69" target="_blank"
                        title="We are Here" class="text-decoration-none text-dark">          
                        <svg width="3em" height="3em" viewBox="0 0 16 16" class="bi bi-geo-alt-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                        </svg>
                    </a>
                <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2570.89181545652!2d-97.18663338428911!3d49.88205817940102!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x52ea738624b2e34d%3A0x34e616d6953cb992!2sWinnipeg%2C%20MB%20R3G%202V8!5e0!3m2!1sen!2sca!4v1604106791460!5m2!1sen!2sca" 
                    width="400" height="300" frameborder="0" style="border:0;" allowfullscreen="" 
                    aria-hidden="false" tabindex="0">
                </iframe> -->
                    <p>Winnipeg, MB R3G 2V8</p>
                </li>

                <li><i class="fas fa-phone mt-4 fa-2x mb-4"></i>
                    <a title="Contact Us" href="tel:1-204-599-8236" class="text-decoration-none text-dark">
                        <svg width="3em" height="3em" viewBox="0 0 16 16" class="bi bi-telephone-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M2.267.98a1.636 1.636 0 0 1 2.448.152l1.681 2.162c.309.396.418.913.296 1.4l-.513 2.053a.636.636 0 0 0 .167.604L8.65 9.654a.636.636 0 0 0 .604.167l2.052-.513a1.636 1.636 0 0 1 1.401.296l2.162 1.681c.777.604.849 1.753.153 2.448l-.97.97c-.693.693-1.73.998-2.697.658a17.47 17.47 0 0 1-6.571-4.144A17.47 17.47 0 0 1 .639 4.646c-.34-.967-.035-2.004.658-2.698l.97-.969z"/>
                        </svg>
                    </a>
                    <p>+1 204 599 8236</p>
                </li>

                <li><i class="fas fa-envelope mt-4 fa-2x"></i>
                    <a title="Email Us" href="mailto:anu.kul0684@gmail.com" class="text-decoration-none text-dark">
                        <svg width="3em" height="3em" viewBox="0 0 16 16" class="bi bi-envelope-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555zM0 4.697v7.104l5.803-3.558L0 4.697zM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757zm3.436-.586L16 11.801V4.697l-5.803 3.546z"/>
                        </svg>
                    </a>
                    <p>anu.kul0684@gmail.com</p>
                </li>
            </ul>
        </div>
        <!--Grid column-->

    </div>
    </header>
@stop()