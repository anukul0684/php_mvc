<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    
    <style>
        body {
            background-image: url('/images/background1.jpg');
            background-repeat: no-repeat;
            background-size: cover;
        }
        
        div#contact_page, div#contact_form, div#contact_details {
            box-sizing: border-box;
        }

        
    </style>
            
</head>
<body class="mt-3 pt-4">
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v12.0&appId=176770547996787&autoLogAppEvents=1" nonce="PRn54w2N"></script>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm fixed-top">
            <div class="container">
                <a class="navbar-brand lead text-danger" href="{{ url('/') }}">
                    <strong>{{ config('app.name', 'Laravel') }}</strong>
                </a>
                <button class="navbar-toggler" type="button" 
                    data-toggle="collapse" data-target="#navbarSupportedContent" 
                    aria-controls="navbarSupportedContent" aria-expanded="false" 
                    aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">                   

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto lead text-dark">
                        <li class="nav-item">
                            <a class="nav-link text-dark" href="/">
                                <strong>Home</strong>
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>          
                        <li class="nav-item">
                            <a class="nav-link text-dark" href="/about">
                                <strong>About</strong>
                            </a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link text-dark" href="/planets">
                                <strong>Planets</strong>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-dark" href="/contact">
                                <strong>Contact</strong>
                            </a>
                        </li>
                        @guest
                            <li class="nav-item">
                                <a class="nav-link text-dark" href="{{ route('login') }}">
                                    <strong>{{ __('Login') }}</strong>
                                </a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link text-dark" href="{{ route('register') }}">
                                        <strong>{{ __('Register') }}</strong>
                                    </a>
                                </li>
                            @endif
                        @else                            
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle text-dark" 
                                    href="#" role="button" data-toggle="dropdown" 
                                    aria-haspopup="true" aria-expanded="false" v-pre>
                                    <strong>{{ Auth::user()->name }}</strong>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" 
                                        aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item text-dark" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                        <strong>{{ __('Logout') }}</strong>
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" 
                                            method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                            @if(Auth::user()->is_admin)
                            <li class="nav_item">
                                <a class="nav-link text-dark" href="/admin">
                                    <strong>Admin Dashboard</strong>
                                </a>
                            </li>
                            @endif
                        @endguest
                        <!-- @auth
                            <li class="nav-item"><a class="nav-link" href="#">Hey! I am logged in</a></li>
                        @endauth -->
                    </ul>
                </div>
            </div>
        </nav>
        <section class="container mt-4 mb-2 badge-dark">
            <div class="py-4">
            @include('partials/flash')
                @yield('content')
            </div>
        </section>
    </div>
    <!-- Footer -->
    <footer class="py-5 bg-dark">
        <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Planets 2020</p>
        </div>
        <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="/js/app.js"></script>
    <script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap.bundle.min.js"></script>
    <script>
        // window.onload=function(){
        //     //$("#leftDiv").height($("#rightdiv").height());
        //     var right=document.getElementById('rightdiv').style.height;
        //     var left=document.getElementById('leftdiv').style.height;
        //     // console.log(right);
        //     // console.log(left);
        //     if(left>right)
        //     {
        //         document.getElementById('rightdiv').style.height=left;
        //     }
        //     else
        //     {
        //         document.getElementById('leftdiv').style.height=right;
        //     }
        // };
    </script>
</body>
</html>